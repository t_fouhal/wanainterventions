<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'gender'=>$faker->randomElement(['male','female']),
        'address'=>$faker->address,
        'position'=>$faker->jobTitle,
       "birth_date"=>$faker->dateTimeBetween('1960-05-1','1993-05-1')->format('Y-m-d'),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Contrat::class, function (Faker $faker) {
    $clients=[];$intervenants=[];
    foreach(\App\Client::all() as $client){
        $clients[]=$client->id;
    }
    foreach(\App\User::all() as $intervenant){
        $intervenants[]=$intervenant->id;
    }
    return [
        'days_nbr' => $faker->unique()->numberBetween(1,10),
        'frequency' => $faker->randomElement(['d','w','m']),
        'client_id'=>$faker->randomElement($clients),
        'principal_intervenant'=>$faker->randomElement($clients),
        'backup_intervenant'=>$faker->randomElement($intervenants),
        'start'=>$faker->date,
        'end'=>$faker->date
    ];
});

$factory->define(\App\Occupation::class, function (Faker $faker) {
    $users=[]; $status=[];
    foreach(\App\User::all() as $user){
        array_push($users,$user->id);
    }
    foreach(\App\Client::all() as $client){
        array_push($status,$client->id);
    }

    $status=array_merge($status,[0,-1,-2,-3,-4,-5,-6]);
    return [
        'date' => $faker->dateTimeBetween('2018-05-1','2019-05-1')->format('Y-m-d'),
        'period' => $faker->randomElement(['am','pm','j']),
        'user_id' => $faker->randomElement($users),
        'status'=>$faker->randomElement($status)
    ];
});


$factory->define(\App\Intervention::class, function (Faker $faker) {
     $clients=[];

     foreach(\App\Occupation::all() as $ocupation){
        if($ocupation->status > 1) array_push($clients,$ocupation->status);
    }

    return [
        'client_id' => $faker->randomElement($clients),
        'period' =>$faker->randomElement(['am','pm','j']),
        'date'=>$faker->dateTimeBetween('2018-05-1','2019-05-1')->format('Y-m-d'),
        'status'=>$faker->randomElement([0,1,2])
    ];
});



$factory->define(\App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        "description"=>$faker->text(40),
        'photo'=>$faker->imageUrl('company')
    ];
});
