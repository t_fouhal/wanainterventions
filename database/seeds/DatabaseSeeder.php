<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(UsersTableSeeder::class);
         factory(\App\User::class, 10)->create();
         factory(\App\Client::class, 5)->create();
         factory(\App\Contrat::class, 10)->create();

         factory(\App\Occupation::class, 50)->create();
         

        $interventions=factory(\App\Intervention::class, 30)->create();
        $intervenants=[];
        foreach (\App\User::all() as $intervenant){
            array_push($intervenants,$intervenant);
        }
        foreach ($interventions as $intervention){
            $randomIntervenant=array_random($intervenants);
            $randomIntervenant->interventions()->attach($intervention->id);
        }
    }
}
