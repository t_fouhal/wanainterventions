<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('period');
            $table->unsignedInteger('user_id');
            $table->integer('status')->default(0);
             /*
                {key:'bureau',value:0},
                {key:'maladie',value:-1},
                {key:'absence non justifiée',value:-2},
                {key:'absence justifiée',value:-3},
                {key:'congé',value:-4},
                {key:'formation',value:-5}
             */ 
            // any other number represent the clientId the user was with
            $table->unique(['period','user_id','date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occupations');
    }
}
