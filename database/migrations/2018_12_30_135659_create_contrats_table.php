<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->decimal('days_nbr');
            $table->string('frequency');        // per day,month, or per week
            $table->date('start');
            $table->date('end');
            $table->integer('principal_intervenant');
            $table->integer('backup_intervenant');
            $table->unique(['frequency','client_id','date','start','end']);
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrats');
    }
}
