<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{

    protected $guarded=[];
    protected $hidden=[];
    protected $appends=['string_status','user_name'];
    public $timestamps=false;

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            if($model->status >0){
                $intervention=Intervention::where(['client_id'=>$model->status,
                                        'date'=>$model->date,
                                        "period"=>$model->period])->first();
                if($intervention){
                    $intervention->intervenants()->detach($model->user_id);
                }
            }
        });

    }  

    public function getStringStatusAttribute(){
        if($this->status >0 ){
            return $this->hasOne('App\Client','id','status')->first()->name;
        }
        switch($this->status){
            case 0  : return "WANA";
            case -1 : return "maladie";
            case -2 : return "absence non autorisée";
            case -3 : return "absence autorisée";
            case -4 : return "congé";
            case -5 : return "formation";
        }

    }

    public function user(){
        //dd($this->belongsTo(User::class)->getBindings());
        return $this->belongsTo(User::class);
    }

    public function getUserNameAttribute(){
        return $this->user()->first()->name;
    }

}
