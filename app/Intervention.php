<?php

namespace App;

use App\Extensions\Filtrable;
use Illuminate\Database\Eloquent\Model;

class Intervention extends Model
{
    use Filtrable;
    protected $guarded=[];
    protected $hidden=['pivot'];
    protected $appends=['client_name'];
    public $timestamps=false;


    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $intervenants=$model->intervenants()->get();
            foreach($intervenants as $intervenant){
                $intervenant->occupations()
                ->where(['status'=>$model->client_id,'date'=>$model->date,'period'=>$model->period])
                ->each(function($occupation){
                    $occupation->delete();
                });
            }
        });

    }
    /*public function getIntervenantsAttribute(){
        //dd($this->Intervenants()->toSql());
        return $this->Intervenants()->get();
    }*/
   /* public function getPeriodeAttribute(){
        return Periode::whereId($this->periode_id)->first();
    }*/
    public function Intervenants(){
        //dd($this->belongsToMany(User::class,'intervenant_intervention','id','intervenant_id')->get());
        return $this->belongsToMany(User::class);
    }
   
    public function Client(){
        return $this->belongsTo(Client::class);
    }
    public function getClientNameAttribute(){
       return $this->Client()->first()->name;
   }
   /* public function getClientAttribute(){
        return $this->Client()->first();
    }*/
    public function getIntervenantsStringAttribute(){
        $intervenantsString='';
        $intervenants=$this->Intervenants()->get();
        $size=sizeof($intervenants);
        foreach ($intervenants as $key => $intervenant) {
            if($key!=$size-1){
                $intervenantsString.=$intervenant->name.',';
            }
            else $intervenantsString.=$intervenant->name;       
        }
        return $intervenantsString;
    }
    public function getStatusStringAttribute(){
        switch($this->status){
            case 0 : return 'nouuvelle'; break;
            case 0 : return 'completée'; break;
            case 0 : return 'reportée'; break;
        }
    }
    public function getDecimalPeriodAttribute(){
        if($this->period=='j') return 1;
        else return 0.5;
    }



}
