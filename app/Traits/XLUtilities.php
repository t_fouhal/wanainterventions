<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;



trait XLUtilities {
   
    public function getColumnString($num){
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getColumnString($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    public function generateTimeHeader($spreadsheet,$periodsFormatted){
     $periodKey=0; 
    $column=1;
    
    while($periodKey<sizeof($periodsFormatted)){
        $subdate=date("d/m", strtotime($periodsFormatted[$periodKey]));
        //set date
        $spreadsheet->setActiveSheetIndex(0)
             ->setCellValue($this->getColumnString($column).'1', $subdate);

        $spreadsheet->getActiveSheet()->getStyle($this->getColumnString($column).'1')->getAlignment()->setHorizontal('center');
             
        //merge with the adjacent cell 
        $spreadsheet->getActiveSheet()->mergeCells($this->getColumnString($column).'1:'.$this->getColumnString($column+1).'1'); 
   
        //auto resize column
        $spreadsheet->getActiveSheet()->getColumnDimension($this->getColumnString($column))->setAutoSize(true);
        //horizontal center cell values
        $spreadsheet->getActiveSheet()->getStyle($this->getColumnString($column))->getAlignment()->setHorizontal('center');

        //setting periods        
       $spreadsheet->setActiveSheetIndex(0)
             ->setCellValue($this->getColumnString($column).'2','AM'); 

        $spreadsheet->setActiveSheetIndex(0)
             ->setCellValue($this->getColumnString($column+1).'2','PM');   
        
        $periodKey+=1;
        $column+=2;
    }
    
    }


    public function downloadXL($filename,$spreadsheet,$format){
                // Redirect output to a client’s web browser (Xlsx)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'.'.lcfirst($format).'"');
                header('Cache-Control: max-age=0');
                // If file served to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');
                
                $writer = IOFactory::createWriter($spreadsheet, $format);
                $writer->save('php://output');
                exit;
    }

    public function genDaysArray($start,$end){
        $periods=new \DatePeriod(
            new \DateTime($start),
            new \DateInterval('P1D'),
            new \DateTime($end)
        );
        $periodsFormatted=[];
        foreach($periods as $period){
            $periodsFormatted[]=$period->format('Y-m-d');
        }
        return $periodsFormatted;
    }
}