<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;


trait Filtrable {
    public function filterPerformer($filterArray){
        $table=is_string($filterArray['table'])?DB::table($filterArray['table']):$filterArray['table'];
        $queryBuilder=$table->selectRaw(implode(',',$filterArray['columns']));

        foreach($filterArray['joins'] as $join){
            $queryBuilder=$queryBuilder->join($join[0],$join[1],'=',$join[2]);
        }
        
        foreach($filterArray['conditionals'] as $conditional){
            $queryBuilder=$queryBuilder->where($conditional[0],$conditional[1],$conditional[2]);
        }

        foreach($filterArray['groupBy'] as $item){
            $queryBuilder=$queryBuilder->groupBy($item);
        }
        if(sizeof($filterArray['orderBy'])){
            $queryBuilder=$queryBuilder->orderBy($filterArray['orderBy'][0],$filterArray['orderBy'][1]);
        }
       
        return $queryBuilder->get()->toArray();
    }
    public function initiateFilterArray(){
        return [
                "table"=>'',
                "columns"=>[],
                "joins"=>[],
                "conditionals"=>[],
                "groupBy"=>[],
                "orderBy"=>[],
                "appends"=>[]
            ];
    }
}