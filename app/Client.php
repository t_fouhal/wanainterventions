<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded=[];
    protected $hidden=['contrat_id'];
    protected $appends=['contrat'];
    public $timestamps=false;


    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $contracts=$model->contracts()->get();
            foreach($contracts as $contract){
                $contract->delete();
            }
        });

    }    

    public function getContratAttribute(){
       return Contrat::whereId($this->contract_id)->first();
    }
    public function contrats(){
        return $this->hasMany(Contrat::class);
    }
    public function Interventions(){
        return $this->hasMany(Intervention::class);
    }

    public function getInterventionsAttribute(){
        return $this->Interventions()->get();
    }
   /* public function getInterventionsRestantesAttribute(){
        return sizeof($this->interventions);//.......not done yet
    }*/

}
