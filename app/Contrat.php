<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    protected $guarded=[];
    protected $hidden=['created_at','updated_at'];
    protected $appends=['client_name','is_active'];
    protected $table = 'contracts';
    public $timestamps=false;

  
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $reports=$model->reports()->get();
            foreach($reports as $report){
                $report->delete();
            }
        });

    }  


    public function client(){
        return $this->belongsTo(Client::class);
    }
    public function principal(){
        return $this->hasOne(User::class,'id','principal_intervenant');
    }

    public function reports(){
        return $this->hasMany(Report::class,'contract_id');
    }
    
    public function getClientNameAttribute(){
        return $this->client()->first()->name;
    }

    public function backupIntervenant(){
        return $this->hasOne(User::class,'id','backup_intervenant');
    }

    public function getIsActiveAttribute()
    {
        $today=date('Y-m-d');
        return $today >= $this->start && $today <= $this->end;
    }


}
