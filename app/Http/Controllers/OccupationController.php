<?php

namespace App\Http\Controllers;

use App\Occupation;
use Illuminate\Http\Request;
use App\Http\Middleware\IsAdmin;


class OccupationController extends Controller
{
    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'user_id'=>'required|exists:users,id',
            'date'=>'required|date',
            'period'=>'required|in:am,pm,j',
            'status'=>'required|date'
        ])->validate();

        return [
            "created"=>Occupation::create($request()->all())->save()
        ];
            
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ["data"=>Occupation::whereId($id)->with(['status'])->get()];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function edit(Timesheet $timesheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timesheet $timesheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occupation $occupation)
    {
        return ["deleted"=>$occupation->delete()];
    }
}
