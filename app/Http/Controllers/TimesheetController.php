<?php

namespace App\Http\Controllers;

use App\Occupation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\IsAdmin;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Intervenant;

class TimesheetController extends Controller
{
    use \App\Traits\XLUtilities;

    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('user')){
            if(request('s')){
                //if(request('sY')!=request('eY')){}
                return ["data"=> User::whereId(request('user'))
                        ->first()->occupations()
                        ->whereBetween('date',[request('s'),request('e')])
                        ->get()];
            }
            return ["data"=> User::whereId($id)->first()->occupations()->get()];
        }
        if(request('paginated')){
            $occupation=new Occupation();
            if(request('user_id')) $occupation=$occupation->where('user_id',request('user_id'));
            return ["data"=> $occupation->orderBy('date','DESC')->paginate(10)];
       }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request=$request->all();
        Validator::make($request,[
            'user_id'=>'required|exists:users,id',
            'date'=>'required|date',
            'period'=>'required|in:am,pm,j',
            'status'=>'required'
        ])->validate();
        return [
            "created"=> Occupation::create($request)->save()
        ];
            
    }


    public function export(){
        $request=request()->all();
        Validator::make($request,[
            'start'=>'required|date',
            'end'=>'required|date',
            'format'=>'required'
        ])->validate();

        $occupations=Occupation::whereBetween('date',[$request['start'],$request['end']])->groupBy(['user_id','status','date'])->get(['user_id','status','date']);
        $periodsFormatted=$this->genDaysArray($request['start'],$request['end']);
  
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setTitle('Timesheet '.$request['start'].' - '.$request['end']);
 
        $this->generateTimeHeader($spreadsheet,$periodsFormatted);

    foreach(User::all() as $userKey => $user){
        $userKey=$userKey+3;
        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$userKey, $user->name);        
                $periodKey=0; 
               // $columnKey=
        while($periodKey<sizeof($periodsFormatted)){
            $sheetPeriodKey=$periodKey*2+1;
            $occupation=Occupation::where(["user_id"=>$user->id,"date"=>$periodsFormatted[$periodKey]])->first();
            if($occupation){
                if($occupation->period=='j'){
                $spreadsheet->setActiveSheetIndex(0)
                         ->setCellValue($this->getColumnString($sheetPeriodKey).$userKey, $occupation->string_status);
                
                         $sheetPeriodKey++;
              $spreadsheet->getActiveSheet()->mergeCells($this->getColumnString($sheetPeriodKey-1).$userKey.':'.$this->getColumnString($sheetPeriodKey).$userKey);         
                }else{
                if($occupation->period=='am'){
                    $spreadsheet->setActiveSheetIndex(0)
                         ->setCellValue($this->getColumnString($sheetPeriodKey).$userKey, $occupation->string_status);
                }
                $sheetPeriodKey++;
                if($occupation->period=='pm'){
                    $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue($this->getColumnString($sheetPeriodKey).$userKey, $occupation->string_status);               
                    }
                }
            }
            $periodKey++;
            
        }
               
        
    }

    //$spreadsheet->getActiveSheet()->getCell('A2')->getValue());
        $this->downloadXL($request['start'].' '.$request['end'],$spreadsheet,request('format'));

    }




    public function indexCount(){
        $request=request()->all();
        
        Validator::make($request,[
            'from'=>'date',
            'to'=>'date'
            ])->validate();

            $filterArray=$this->initiateFilterArray();
            $filterArray['table']='interventions';
            $filterArray['columns'][]="count('*') as count";
            $filterArray['orderBy']=['count','DESC'];
          foreach($request as $userKey=>$value){
              switch($userKey){
                  case 'client_id': {
                      array_push($filterArray['columns'],'clients.name,client_id'); 
                      array_push($filterArray['joins'],['clients','interventions.client_id','clients.id']); 
                      $filterArray['groupBy']=array_merge($filterArray['groupBy'],['client_id','clients.name']);
                      break;
                    }
                  case 'user_id':  { 
                      array_push($filterArray['columns'],'users.name');
                      
                      $filterArray['joins']=array_merge($filterArray['joins'],[
                                            ['intervention_user','interventions.id','intervention_user.intervention_id'],
                                            ['users','intervention_user.user_id','users.id']
                                        ]);
                       $filterArray['groupBy']=array_merge($filterArray['groupBy'],['user_id','users.name']);                
                       
                       break;
                    }
                  case 'status': {
                      array_push($filterArray['columns'],
                      "(case
                        when status=0 then 'nouvelle'
                        when status=1 then 'completée'
                        when status=2 then 'reportée' 
                        end )  as name"
                    );
                      array_push($filterArray['groupBy'],'status'); break;
                    }
                case 'period':{
                    array_push($filterArray['groupBy'],'period'); break;
                }  
                 case 'from':{
                     array_push($filterArray['conditionals'],['date','>=',$value]);
                     break;
                 }
                 case 'to':{
                    array_push($filterArray['conditionals'],['date','<=',$value]);
                    break;
                }
              }
              if(!in_array($userKey,['from','to']) && $value) array_push($filterArray['conditionals'],[$userKey,'=',$value]);
          }
        
          return response()
          ->json($this
          ->filterPerformer($filterArray));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ["data"=>Occupation::whereId($id)->first()];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function edit(Timesheet $timesheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Occupation $occupation)
    {
        $request=$request->all();
        Validator::make($request,[
            'date'=>'date',
            'period'=>'in:am,pm,j'
        ])->validate();

        return [
            "updated"=>$occupation->update($request)
        ];        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occupation $occupation)
    {
        return ["deleted"=>$occupation->delete()];
    }



}
