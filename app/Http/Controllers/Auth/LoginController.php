<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
//use Zend\Diactoros\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
    /*public function login(Request $request){
        dd('right method');
        $request->validate([
            'email' => 'required|email|string',
            'password' => 'required|string',
        ]);
        $user=\App\User::where(["email"=>$request->email,"password"=>$request->password])->first();
        if($user){
            Auth::login($user);
            return $this->sendLoginResponse($request);
        }

    }*/



    

    protected function authenticated(Request $request, $user)
    {
        return redirect('/');
    }

}
