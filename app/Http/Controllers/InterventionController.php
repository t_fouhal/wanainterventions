<?php

namespace App\Http\Controllers;

use App\Client;
use App\Intervenant;
use App\Intervention;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Nexmo\Message\Query;
use App\Http\Middleware\IsAdmin;

class InterventionController extends Controller
{
    use \App\Traits\Filtrable;


    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $interventions = new Intervention();
        if(request('user_id')){
            $interventions= User::whereId(request('user_id'))->first()->interventions();
        }
        $tableColumns=Schema::getColumnListing('interventions');
        foreach(request()->all() as $key => $value){
            if(in_array($key,$tableColumns)){ 
                $interventions=$interventions->where([$key=>$value]);
            }
        }
        if(request('s')){
            $interventions=$interventions->whereBetween('date',[request('s'),request('e')]);
        }
        if(request('client_id') ){
            return response()->json(["data"=>$interventions->get()->each->append('intervenants_string')]); 
        }
        if(request('full')){
            return response()->json(["data"=>$interventions->orderBy('date','DESC')->with(['intervenants'])->paginate(10)]);
        }
  
             return response()->json(["data"=>$interventions->get()]);
    }

    public function indexCount(){
    
        $request=request()->all();
        
        Validator::make($request,[
            'from'=>'date',
            'to'=>'date'
        ])->validate();

            $filterArray=$this->initiateFilterArray();
            $filterArray['table']='interventions';
            $filterArray['columns'][]="count('*') as count,(case
                                                                when status=0 then 'nouvelle'
                                                                when status=1 then 'completée'
                                                                when status=2 then 'reportée' 
                                                                end )  as status ";
            $filterArray['orderBy']=['count','DESC'];
            $filterArray['groupBy'][]='interventions.status';
          foreach($request as $key=>$value){
              switch($key){
                  case 'client_id': {
                      array_push($filterArray['columns'],'clients.name,client_id'); 
                      array_push($filterArray['joins'],['clients','interventions.client_id','clients.id']); 
                      $filterArray['groupBy']=array_merge($filterArray['groupBy'],['client_id','clients.name']);
                      break;
                    }
                  case 'user_id':  { 
                      array_push($filterArray['columns'],'users.name');
                      
                      $filterArray['joins']=array_merge($filterArray['joins'],[
                                            ['intervention_user','interventions.id','intervention_user.intervention_id'],
                                            ['users','intervention_user.user_id','users.id']
                                        ]);
                       $filterArray['groupBy']=array_merge($filterArray['groupBy'],['user_id','users.name']);                
                       
                       break;
                    }
                  case 'status': {
                      array_push($filterArray['columns'],
                      "(case
                        when status=0 then 'nouvelle'
                        when status=1 then 'completée'
                        when status=2 then 'reportée' 
                        end )  as name"
                    );
                      array_push($filterArray['groupBy'],'status'); break;
                    }
                case 'period':{
                    array_push($filterArray['groupBy'],'period'); break;
                }  
                 case 'from':{
                     array_push($filterArray['conditionals'],['date','>=',$value]);
                     break;
                 }
                 case 'to':{
                    array_push($filterArray['conditionals'],['date','<=',$value]);
                    break;
                }
              }
              if(!in_array($key,['from','to']) && $value) array_push($filterArray['conditionals'],[$key,'=',$value]);
          }
        
          return response()
          ->json($this
          ->filterPerformer($filterArray));
        
    }

    public function daysCount(){
        $request=request()->all();
        
        Validator::make($request,[
            'from'=>'date',
            'to'=>'date'
        ])->validate();

            $filterArray=$this->initiateFilterArray();
            $filterArray['table']='interventions';
            $filterArray['columns'][]="sum((case
                                            when period='am' then 0.5
                                            when period='pm' then 0.5
                                            when period='j' then 1 
                                            end ) ) as count ,(case
                                                                when status=0 then 'nouvelle'
                                                                when status=1 then 'completée'
                                                                when status=2 then 'reportée' 
                                                                end )  as status";
            $filterArray['orderBy']=['count','DESC'];
            $filterArray['groupBy'][]='interventions.status';

          foreach($request as $key=>$value){
              switch($key){
                  case 'client_id': {
                      array_push($filterArray['columns'],'clients.name,client_id'); 
                      array_push($filterArray['joins'],['clients','interventions.client_id','clients.id']); 
                      $filterArray['groupBy']=array_merge($filterArray['groupBy'],['client_id','clients.name']);
                      break;
                    }
                  case 'user_id':  { 
                      array_push($filterArray['columns'],'users.name');
                      
                      $filterArray['joins']=array_merge($filterArray['joins'],[
                                            ['intervention_user','interventions.id','intervention_user.intervention_id'],
                                            ['users','intervention_user.user_id','users.id']
                                        ]);
                       $filterArray['groupBy']=array_merge($filterArray['groupBy'],['user_id','users.name']);                
                       
                       break;
                    }
                  case 'status': {
                      array_push($filterArray['columns'],
                      "(case
                        when status=0 then 'nouvelle'
                        when status=1 then 'completée'
                        when status=2 then 'reportée' 
                        end )  as name"
                    );
                      array_push($filterArray['groupBy'],'status'); break;
                    }
                case 'period':{
                    array_push($filterArray['groupBy'],'period'); break;
                }  
                 case 'from':{
                     array_push($filterArray['conditionals'],['date','>=',$value]);
                     break;
                 }
                 case 'to':{
                    array_push($filterArray['conditionals'],['date','<=',$value]);
                    break;
                }
              }
              if(!in_array($key,['from','to']) && $value) array_push($filterArray['conditionals'],[$key,'=',$value]);
          }
        
          return response()
          ->json($this
          ->filterPerformer($filterArray));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return ["intervenants"=>Intervenant::all(),
                 "clients"=>Client::all()];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Validator::make($request->all(),[
            'client_id'=>'required|exists:clients,id',
            'status'=>'required|numeric',
            'date'=>'required|date',
            'period'=>'required|in:am,pm,j'
        ])->validate();
        $intervention=Intervention::create(array_except($request->all(),'intervenants[]'));
      
             $this->attachIntervenants($request,$intervention->id);
        

        return $intervention;
    }

    public function attachIntervenants(Request $request,$id) {
        if(!isset($request->all()['intervenants[]'])) return; 
        Validator::make($request->all(),[
            'intervenants[]'=>'array',
            'intervenants[].*'=>'exists:users,id'
        ])->validate();

        $intervention= Intervention::whereId($id)->first();
        $attachedIntervenants=$request->all()['intervenants[]'];
        $intervenants=$intervention->intervenants();

        //old Intervenants Ids
        $oldIntervenantsIds=[];
        foreach($intervenants->get() as $oldIntervenant){
            array_push($oldIntervenantsIds,$oldIntervenant->id);
        }

        //creating records in the occupations table for the attached intervenants
        foreach($attachedIntervenants as $attachedIntervenantId){
            if(!in_array($attachedIntervenantId,$oldIntervenantsIds))
                User::whereId($attachedIntervenantId)
                            ->first()
                            ->occupations()
                            ->create([
                                    "status"=>$intervention->client_id,
                                    "period"=>$intervention->period,
                                    "date"=>$intervention->date
                            ])->save();
        }


        //deleting excluded intervenants from occupations table
        foreach($intervenants->get() as  $intervenant){
                if(!in_array($intervenant->id,$attachedIntervenants)){
                    $intervenant->occupations()
                            ->where('status',$intervention->client_id)
                            ->get()
                            ->each->delete();
                }
        }
        //setting the new intervenants for this intervention
           $attached=$intervenants->sync($request->all()['intervenants[]']);

        return json_encode(["attached"=>$attached]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        return ["data"=>Intervention::whereId($id)
                                    ->with(['intervenants','client'])
                                    ->first()];
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ["data"=>Intervention::whereId($id)->with('intervenants')->first(),
                "AllIntervenants"=>Intervenant::all(),
                "clients"=>Client::all()];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Intervention $intervention)
    {
        $this->attachIntervenants($request,$intervention->id);
        return ["updated"=>$intervention
                            ->update(array_except($request->all(),'intervenants[]'))];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function destroy(Intervention $intervention)
    {
        if($intervention)
            return ["deleted"=> $intervention->delete()];

    }
}
