<?php

namespace App\Http\Controllers;

use App\Contrat;
use App\Periode;
use Illuminate\Http\Request;
use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\Validator;

class ContratController extends Controller
{
    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('paginated')){
            $contracts=new Contrat();
            if(request('client_id')){
                $contracts=$contracts->where('client_id',request('client_id'));
            }
            return response()->json(["data"=>$contracts->paginate(10)]);
        }
        return response()->json(["data"=>Contrat::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json(["periodes"=>Periode::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request=$request->all();

        Validator::make($request,[
            'frequency'=>'required|in:w,d,m,y',
            'client_id'=>'required|exists:clients,id',
            'days_nbr'=>'required|numeric',
            'start'=>'required|date',
            'end'=>'required|date'
        ])->validate();
        $request['days_nbr']=floatval($request['days_nbr']);
        return ["created"=>Contrat::create($request)->save()];
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(["data"=>Contrat::whereId($id)->first()]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function edit(Contrat $contrat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request=request()->all();

        Validator::make($request,[
            'frequency'=>'in:w,d,m,y',
            'client_id'=>'exists:clients,id',
            'days_nbr'=>'numeric',
            'start'=>'date',
            'end'=>'date'
        ])->validate();
        if(isset($request['days_nbr'])){
            $request['days_nbr']=floatval($request['days_nbr']);
        }

        return ["updated"=>Contrat::whereId($id)->first()->update($request)];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contrat $contrat)
    {
        //
    }
}
