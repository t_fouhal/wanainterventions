<?php

namespace App\Http\Controllers;

use App\Intervenant;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\IsAdmin;

class IntervenantController extends Controller
{

    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(request('paginated')){
			return ["data"=>User::paginate(10)];
		}
        return ["data"=>User::all()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestArray=$request->all();
        Validator::make($requestArray,[
            'email'=>'required|email|unique:users',
            'name'=>'required|max:254|unique:users',
            'password'=>'required|min:6'
        ])->validate();

        $requestArray['password']=Hash::make($requestArray['password']);
        return Intervenant::create($requestArray);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

                if(request('full')){
                    //getting and appending all involved intervenants
                    $intervenant=User::whereId($id)->first();
                    $interventions=[];
                    //paginating interventions to avoid returning big chuncks of data at once
                    $paginatedInterventions=$intervenant->interventions()->paginate(10);

                    foreach($paginatedInterventions as $intervention){
                    //creating and filling a new array of all involved intervenants
                        $intervenants=[];
                        foreach($intervention->intervenants()->get() as $withIntervenant){
                            //excluding the current intervenant
                           if($withIntervenant->id != $id){
                                $intervenants[]=$withIntervenant;
                            }
                        }
                        $interventions[]=["intervenants"=>$intervenants,
                                            "intervention"=>$intervention];
                    }
                    $data=[ "data"=>$intervenant ];
                     $data["fullInterventions"]=$interventions;
                     //adding pagination infos to the view for the paginantion component
                    $data["pagination"]=$paginatedInterventions;
                }else{
                    $intervenant=User::whereId($id)->with('interventions')->first();
                    $data=[ "data"=>$intervenant ];
                }
                return $data;


    }


    public function occupations($id){
  
        if(request('s')){
            //if(request('sY')!=request('eY')){}
            return ["data"=> User::whereId($id)
                    ->first()->occupations()
                    ->whereBetween('date',[request('s'),request('e')])
                    ->get()];
        }
        return ["data"=> User::whereId($id)->first()->occupations()->get()];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            return $this->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $requestArray=$request->all();
     
        if(isset($requestArray['password'])){
            $requestArray['password']=Hash::make($requestArray['password']);
        }
        return ["updated"=>User::whereId($id)
                                ->first()
                                ->update($requestArray)];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
            return ["deleted"=> $user->delete()];
    }

}