<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use App\Contrat;
use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request=request()->all();
        //dd($request);
        if(isset($request['contract_id'])){
            return ["data"=>Contrat::whereId($request['contract_id'])
                                ->first()
                                ->reports()
                                ->paginate(10)];
        }
        if(isset($request['for_notifications'])){  
            return ["data"=>Report::whereNull('destination')->where('date','<=',date('Y-m-d'))
                            ->with(['client'])
                            ->get()->each->append('client_name')];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'date'=>'required|date'
        ])->validate();
  
        if(request('contract_id')){
            $request=array_except($request->all(),'client_id');
            $report=Contrat::whereId(request('contract_id'))->first()->reports();
            if(request('destination')){
                $request["destination"]=$this->uploadReport();
                $report->create($request)->save();
                return redirect()->back(); 
           }
                return ["created"=>$report->create($request)->save()];
        }
            

    }

    public function uploadReport(){
        $directory=public_path('/rapports/'.date('m-Y')."/");
        if(!file_exists($directory)){
            File::makeDirectory($directory);
        }
        $file_path='/rapports/'.date('m-Y')."/".$_FILES['destination']['name'];
        move_uploaded_file($_FILES['destination']['tmp_name'],public_path($file_path));
        return $file_path;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return ["data"=>$report];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $request=$request->all();
        Validator::make($request,[
            'date'=>'date'
        ])->validate();
        if(isset($request['destination'])){
            if($report->destination){
                if(file_exists(public_path($report->destination))) unlink(public_path($report->destination));
            }
            $request['destination']=$this->uploadReport();
            $report->update($request);
            return redirect()->back();
        }
        return ["updated"=>$report->update($request)];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        return ["deleted"=>$report->delete()];
        
    }
}
