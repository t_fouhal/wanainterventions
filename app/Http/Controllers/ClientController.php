<?php

namespace App\Http\Controllers;

use App\Client;
use App\Contrat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Nexmo\Conversations\Collection;
use App\Http\Middleware\IsAdmin;

class ClientController extends Controller
{
    function __construct()
    {
        $this->middleware(IsAdmin::class)->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('paginated')){
            return response()->json(["data"=>Client::paginate(10)]);

        }
        return response()->json(["data"=>Client::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return ["contrats"=>Contrat::with('interventions')->all()];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'name'=>'required'
        ])->validate();

       $client=Client::create($request->all());
       $saved=$client->save();
       
        if($request->get('image')) {
            File::makeDirectory(public_path('\clients\\'.$request->get('name').'\documents'),0777,true,true);
           $photo = file_get_contents($request->get('image'));
           file_put_contents(public_path("\clients\images\\"), $photo);
       }
       return [
        "created"=>$saved
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($client)
    {
        return ["data"=>Client::whereId($client)->first()];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return [
                "data"=>Client::whereId($client)->with(['interventions'])->first(),
                "contrats"=>Contrat::with('interventions')->all()
            ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        return response()->json(["updated"=>$client->update($request->all())]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        return response()->json(["deleted"=>$client->delete()]);
    }
}
