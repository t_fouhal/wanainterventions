<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 30/12/2018
 * Time: 19:29
 */

namespace App\Extensions;


trait Filtrable
{

    public function filterByDate($queryBuilder,$interval){
        foreach (["annee","mois","jour"] as $datePart){
            $queryBuilder=$queryBuilder
                ->whereBetween($datePart,
                    [ $this->getIntervalValue($interval,"from",$datePart),
                        $this->getIntervalValue($interval,"to",$datePart)
                    ]);
        }
        return $queryBuilder;
    }

    //handle all cases : (right or left interval side not provided [open] )
    //ex:if "from" index is not provided this function will set the year to 2000 the month to 1 and the day to 1 by default

    private  function getIntervalValue($array,$side,$label){
        return isset($array[$side])?
            $array[$side][$label]:
            $this->getDefaultDateValue($side,$label);
    }

    private  function getDefaultDateValue($side,$label){
        if($label!="annee") return 1;
        return $side=='from'?2000:2050;
    }


}