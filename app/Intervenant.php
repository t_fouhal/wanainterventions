<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 30/12/2018
 * Time: 14:17
 */

namespace App;


class Intervenant extends User
{
    protected $table = 'users';

    public $is_admin=false;

    protected static function boot()
    {
        parent::boot();
        self::addGlobalScope(function ($query){
            $query->where('is_admin',false);
        });
    }

        


}