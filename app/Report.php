<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $guarded=[];
    public $timestamps=false;



    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            if($model->destination && file_exists(public_path($model->destination))) unlink(public_path($model->destination));
        });

    }  


    public function contract(){
        return $this->belongsTo(Contrat::class,'contract_id');
    }

    public function getClientNameAttribute(){
        return $this->client()->first()->name;
    }

    public function client(){
        return $this->contract()->first()->client();
    }
    
}
