<?php

namespace App;

use App\Intervention;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

   // protected $appends=['disponible'];
    protected $hidden=["password","remember_token","created_at","updated_at","email_verified_at",'pivot'];
    protected $guarded=[];
    public $timestamps=false;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
  
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            foreach($model->occupations()->get() as $occupation){
                $occupation->delete();
            }
        });

    }
 

    public function Interventions(){
       // dd($this->belongsToMany(Intervention::class,'intervenant_intervention','id','intervenant_id')->toSql());
        return $this->belongsToMany(Intervention::class);
    }

    public function getInterventionsAttribute(){
        return $this->interventions();
    }
    public function occupations(){
       return $this->hasMany('App\Occupation');
    }
    public function getOccupationsAttribute(){
         return $this->hasMany('App\Occupation');
    }

    public function getDisponibleAttribute(){
        foreach(self::Indisponibles() as $intervenantDisponible){
            if($intervenantDisponible->id==$this->id) return false;
        }
        return true;
    }
public function getIndisponibleAttribute(){
    foreach(self::Indisponibles() as $intervenantDisponible){
        if($intervenantDisponible->id==$this->id) return true;
    }
}
public static function Indisponibles(){
    $intervenantsIndisponibles=[];
    Intervention::EnCours()->get()->each(function ($intervention) use(&$intervenantsIndisponibles){
        $intervenants=[];
        foreach ($intervention->intervenants()->get() as $intervenant){
            array_push($intervenants,$intervenant);
        }
        $intervenantsIndisponibles=array_merge($intervenants,$intervenantsIndisponibles);
    });
    return $intervenantsIndisponibles;

}
/*    public static function Disponibles(){
    $intervenants=User::all();
    $intervenantsDisponibles=[];
    Intervention::EnCours()->get()->each(function ($intervention) use(&$intervenantsDisponibles){
        $intervention->intervenants()->get()->each(function($intervenant) use(&$intervenantsDisponibles){

        });
    });

}*/


}
