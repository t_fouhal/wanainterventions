<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Fouhal Mohammed Taqi eddine, takifouhal@gmail.com">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="icon" href={{asset("/assets/img/logo.png")}} type="image/x-icon">
    <title>Wana interventions</title>
    <link rel="stylesheet" href={{asset("/css/bundle.css")}}>
   
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
        .backdrop{
            position:fixed;height:100%;width:100%;background-color:rgba(255,255,255,0.3);top:0;left:0;z-index:999998
        }
    </style>
</head>
<body  id="spinner" class="light">
<div id="ploader" class="preloader-wrapper big" style="position:fixed;" >
    <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>
<div id='backdrop' class=""></div>
<div id="app">
</div>

<?php if(isset($globalconf)) echo '<script>'.$globalconf.'</script>' ?>
<script src={{asset("/js/app.js")}}></script>
</body>
</html>