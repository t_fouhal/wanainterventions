import 'fullcalendar';
var w_calender=function(events) {
    $('#calendar').fullCalendar({
      locale:'fr',
      firstDay:0,
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'aujourdhui',
        month: 'mois',
        week : 'semaine',
        day  : 'jour'
      },
      hiddenDays: [ 5, 6 ],
      minTime:'08:00:00',
      maxTime:'17:30:00',
      events    : events,
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      eventDrop:function(event, delta, revertFunc, jsEvent, ui, view ){
        console.log(event)
      }
    })
}
export default w_calender;