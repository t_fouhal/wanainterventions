import React  from 'react';

const TabsHeader=(props)=>{
    return (
        <header className="blue accent-3 relative">
        <div className="container-fluid text-white">
        <div className="row p-t-b-10 ">
            <div className="col">
                <div className="pb-3">
                    {props.hasOwnProperty('img') &&
                        <div className="image mr-3  float-left">
                            <img className="user_avatar no-b no-p" src={window.globalconf.applicationpath+props.img}  />
                        </div>
                    }
                    <div>
                        {props.hasOwnProperty('text')&&
                            <h6 className="p-t-10">{props.text}</h6>
                        }
                        {props.subText}
                    </div>
                </div>
            </div>
        </div>
        {props.children}
        </div>
        </header>
    );
}

export default TabsHeader;

