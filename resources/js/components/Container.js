import React  from 'react';
import Footer from './Footer';


const Container=(props)=>{
    return (
        <div className="page has-sidebar-left">
            {props.hasOwnProperty('title') && props.title &&
            <header className="my-3">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col">
                            <h1 className="s-24">
                                <i className="icon-pages" />
                                {props.title}
                            </h1>
                        </div>
                    </div>
                </div>
            </header>
            }
                {props.children}

        </div>
    );
}

export default Container;

