import React from 'react'

const AnalyticsList=(props)=>{

    let formattedData=[]
    for(let item in props.data){
        item=props.data[item]
        if(formattedData.hasOwnProperty(item.name)){
            formattedData[item.name][item.status]=item.count;
        }
        else
            formattedData[item.name]={[item.status]:item.count}
    }

    let domItems=[];

    for(let key in formattedData){
        let item=formattedData[key];
        domItems.push((<tr>
                            <td>{key}</td>
                            <td>
                                {item.hasOwnProperty('completée')?item.completée:0}
                            </td>
                            <td>
                                {item.hasOwnProperty('nouvelle')?item.nouvelle:0}
                            </td>
                            <td>
                                {item.hasOwnProperty('reportée')?item.reportée:0}
                            </td>
                        </tr>))
    }


    return <div className="box-body">
            <table className="table table-bordered">
            <tbody>
                <tr>
                    <th>{props.for}</th>
                    <th>completées</th>
                    <th>nouvelles</th>
                    <th>reportées</th>
                </tr>
                {domItems}
                
            </tbody>
            </table>
        </div>

}


export default AnalyticsList;