import EditableList from './EditableList';


export default class OccupationsList extends EditableList{
    getPaths=()=>{
        return {
           path:this.props.hasOwnProperty('path')?this.props.path:'/occupations-list',
           entityPath:'/occupations?paginated=true'
           +(this.props.user_id?'&user_id='+this.props.user_id:'')
        }
    }
   
    defineObject=(item)=>{
       return {
           id:item.id,
           text:item.user_name,
           subText:item.string_status,
           info:item.period,
           time:item.date
       }
    }
   
}

