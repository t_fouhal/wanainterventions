import React ,{Component,Fragment}  from 'react';
import InterventionCalender from './InterventionsCalender';
import  TabsHeader from './TabsHeader';
import TabsPages from './TabsPages';
import Tabs from './Tabs';
import Client from './Client';
import ContractsList from './ContractsList';

export default class ClientProfile extends Component{
 
    state={
        data:null
    }

    componentDidMount(){
        this.getClient()
    }
    getClient=()=>{
        let interventions=[];
        SpinnerStart()
         return axios.get('/api/clients/'+this.props.match.params.id)
            .then(({data})=>{
                SpinnerStop()
                this.setState({data:data.data})
            })
     
    }

    render() {
        let tabsPages=[
            {id: "low",
             view: <InterventionCalender 
                    for={"interventions?client_id="+this.props.match.params.id}  
                    user={this.props.match.params.id} 
                    text='intervenants_string'
                    fileAppendName={this.state.data?this.state.data.name:''}
                    />}
        ]
        let tabs=[
            {id: 'low', title: 'Interventions', icon: 'icon icon-schedule'}
        ]

        if(window.globalconf.user.is_admin){
            tabsPages.unshift({id: "profile", view: <Client method='PUT' data={this.state.data}/>});
            tabs.unshift({id: 'profile', title: 'Profile', icon: 'icon icon-user-circle'})

            tabsPages.push({id: "contracts", view: <ContractsList client_id={this.props.match.params.id}  path={'/clients/'+this.props.match.params.id}  page={this.props.match.params.page}/>});
            tabs.push({id: 'contracts', title: 'Contrats', icon: 'icon icon-list2'})
        }

            return <Fragment>
                        <Fragment></Fragment>
                    {this.state.data&&
                        <Fragment>
                            <TabsHeader
                                subText={this.state.data.description}
                                text={this.state.data.name}
                                img={"/assets/img/enterprise.png"}
                                //this.state.data.photo?this.state.data.photo:
                                >
                                <Tabs items={tabs}  />
                            </TabsHeader>
                        <TabsPages
                        pages={tabsPages}
                        />
                        </Fragment>
                    }
                        </Fragment>

    }
}
