import React ,{Component,Fragment}  from 'react';
import TabsHeader from './TabsHeader';
import Tabs from './Tabs';
import TabsPages from './TabsPages';
import '../libs/jquery.responsivetabs';
import List from './List';
import InterventionsList from './InterventionsList';
import OccupationsCalender from'./OccupationsCalender';
import { Route } from 'react-router-dom';

import UserCreation from './UserCreation';


export default class User extends Component{
    state={
        data:null,
        interventions:[],
        pagination:{},
        currentPage:1,
        forceUpdate:false,
    }
    

  

   


    componentDidMount(){
        if(window.globalconf.user.is_admin==1 || window.globalconf.user.id==this.props.match.params.id){
            this.getInterventions()
        }
     }

    getInterventions=()=>{
        let interventions=[];
        let page=this.props.match.params.page?this.props.match.params.page:1;
        SpinnerStart()
         return axios.get('/api/users/'+this.props.match.params.id+"?full=true&page="+page) //TODO:clean the request
            .then(res=>{
                SpinnerStop()
                this.setState({data:res.data.data,interventions,pagination:res.data.pagination,currentPage:window.location.pathname})
            })
     
    }
    shouldComponentUpdate(){
        return this.state.currentPage != window.location.pathname || this.state.forceUpdate;
    }
    componentDidUpdate(){
        this.getInterventions()
    }




 

    render() {
        let tabsPages=[
            {id: "money",
                view: <InterventionsList path={'/users/'+this.props.match.params.id} page={this.props.match.params.page} user_id={this.props.match.params.id} />},
            {id: "low",
             view: <OccupationsCalender for={"occupations?user="+this.props.match.params.id}  user={this.props.match.params.id} 
             fileAppendName={this.state.data?this.state.data.name:''}/>}
        ]
        let tabs=[
            {id: 'money', title: 'Interventions', icon: 'icon icon-tasks'},
            {id: 'low', title: 'Timesheet', icon: 'icon icon-schedule'}
        ]

        if(window.globalconf.user.is_admin){
            tabsPages.unshift({id: "profile", view: <UserCreation method='PUT' data={this.state.data}/>});

            tabs.unshift({id: 'profile', title: 'Profile', icon: 'icon icon-user-circle'})
        }

        return  (
            <Fragment>
                <Fragment></Fragment>
            {this.state.data&&
                <Fragment>
                    <TabsHeader
                        subText={this.state.data.email}
                        text={this.state.data.name}
                        img={"/assets/img/"+this.state.data.gender+".png"}>
                        <Tabs items={tabs}  />
                    </TabsHeader>
                <TabsPages
                pages={tabsPages}
                />
                </Fragment>
            }
                </Fragment>)

    }
}
