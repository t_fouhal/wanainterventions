import React,{Component,Fragment} from 'react';
import DatePicker from './DatePicker';
import Select2 from './Select2';
import TabsHeader from './TabsHeader';


export default class InterventionCreation extends Component{

    state={
        data:null,
        status:0,
        clients:[],
        intervenants:[],
        selectedClient:[],
        selectedIntervenants:[],
        selectedPeriod:[]
        
    }


    componentDidMount(){
        if(this.props.method=='POST'){
            if(this.props.hasOwnProperty('as_model')){
                this.applyModelProperties()
            }else{
                //default status :0 for new intervention
                this.setState({data:{status:0}})
            }
        }
        this.fillSelectBoxes().then(()=>{this.handlePutMethod()})    
    }

    fillSelectBoxes=()=>{
        SpinnerStart()
        return axios.all([(()=>axios.get('/api/clients'))(),
        (()=>axios.get('/api/intervenants'))()])
            .then(axios.spread((clients, intervenants) =>{
            let formattedClients=[],formattedIntervenants=[]; 

                for(let client in clients.data.data){
                    formattedClients.push({
                        key:clients.data.data[client].name,
                        value:clients.data.data[client].id
                    })
                }
                for(let intervenant in intervenants.data.data){
                    formattedIntervenants.push({
                        key:intervenants.data.data[intervenant].name,
                        value:intervenants.data.data[intervenant].id
                    })
                }
            
            this.setState({clients:formattedClients,intervenants:formattedIntervenants})
            SpinnerStop()
            }));
    }

    applyModelProperties=()=>{
        this.setState({data:
            {date:this.props.selectedDate,
                status:0,
                period:this.props.selectedPeriod[0]},
                selectedPeriod:this.props.selectedPeriod})
    }

    handlePutMethod=()=>{
        if(this.props.method=='PUT'){
            SpinnerStart()
            axios.get('/api/interventions/'+this.props.id)
                .then(({data})=>{
                   let selectedIntervenants=[];
                   for (let intervenant in data.data.intervenants){
                        selectedIntervenants.push(data.data.intervenants[intervenant].id)
                   }
                  let selectedClient=[data.data.client_id];

                  let selectedPeriod=[data.data.period]
                  this.setState({selectedIntervenants,selectedClient,selectedPeriod})
                  document.getElementById('date').value=data.data.date;
                  document.querySelector('input[value="'+data.data.status+'"]').checked=true;

                  SpinnerStop()
                })
        }
    }
    submit=(e)=>{
        e.preventDefault()
        SpinnerStart()
        let method=this.props.method.toLowerCase();
        axios[method]('/api/interventions'+(method=='put'?'/'+this.props.id:''),this.state.data)
        .then((res)=>{
                SpinnerStop()
                toastr.success('l\'Intervention '+(method=='put'?'modifiée':'crée')+' avec succés')
                if(this.props.hasOwnProperty('as_model')) this.props.refreshCalender()
        }).catch((err)=>{
            SpinnerStop()
            toastErrors(err)
        })
    }
    onChange=(e)=>{
        let data={};
        for(let input in this.state.data){
            data[input]=this.state.data[input];
        }
        data[e.target.name]=e.target.value;
        this.setState({data})
    }



    
    render(){
        let defaultStatus=this.props.method=='POST'?{checked:true}:{}

            return (<Fragment>
                  <TabsHeader text={(this.props.method=='POST'?'Creation':'Modification')+" d'une intervention"} />  
                    <div className={this.props.hasOwnProperty('as_model')?"col-md-12":"col-md-7  offset-md-2"} >
                        <form onSubmit={this.submit}
                            onChange={this.onChange}
                            onSelect={this.onChange}
                            autoComplete="off"
                            >
                        <div className="card no-b  no-r" >
                            <div className="card-body" style={this.props.hasOwnProperty('as_model')?{border:'2px solid #bdc3c7'}:{}}>
                            <div className="form-row">
                                <div className="col-md-12">
            
                                <div className="form-row">
                                    <div className="form-group col-5 m-0">
                                    <label htmlFor="cnic" className="col-form-label s-12"><i className="icon-fingerprint" />Client</label>
                                        <Select2 
                                            onChange={(e)=>this.onChange(e)}
                                            name='client_id'
                                            id='clientSelect2'
                                            selected={this.state.selectedClient}
                                            items={this.state.clients}
                                        />
                                    </div>
                                    <div className="form-group col-4 m-0">
                                    <label htmlFor="date" className="col-form-label s-12"><i className="icon-calendar mr-2" />Date d'intervention</label>
                                        <DatePicker onChange={this.onChange} name="date" value={this.props.selectedDate}/>
                                    </div>
                                    <div className="form-group col-3 m-0">
                                     <label htmlFor="mobile" className="col-form-label s-12"><i className="icon-user-group mr-2" />periode</label>
                                            <Select2 
                                                onChange={this.onChange}
                                                name='period'
                                                id='periodSelect2'
                                                selected={this.state.selectedPeriod}
                                                items={[
                                                    {key:'avant midi',value:'am'},
                                                    {key:'aprés midi',value:'pm'},
                                                    {key:'journée',value:'j'},                
                                                ]}
                                            />
                                    </div>
                                </div>
                                <div className="form-group col-12 m-0">
                                    <label  className="col-form-label s-12">Status</label>
                                    <br />
                                    <div className="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="new" name="status" className="custom-control-input" value={0}  {...defaultStatus} />
                                        <label className="custom-control-label m-0" htmlFor="new">nouvelle</label>
                                    </div>
                                    <div className="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="completed" name="status" className="custom-control-input" value={1} />
                                        <label className="custom-control-label m-0" htmlFor="completed">completée</label>
                                    </div>
                                    <div className="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="problem" name="status" className="custom-control-input" value={2} />
                                        <label className="custom-control-label m-0" htmlFor="problem">reportée</label>
                                    </div>

                            </div>
                            </div>

                            </div>
                            <div className="form-row mt-1">
                                <div className="form-group col-9 m-0">
                                <label htmlFor="intervenantsSelect2" className="col-form-label s-12"><i className="icon-user-group mr-2" />Intervenants</label>
                                    <Select2 
                                        onChange={(e)=>this.onChange(e)}
                                        multiple='multiple'
                                        name='intervenants'
                                        id='intervenantsSelect2'
                                        selected={this.state.selectedIntervenants}
                                        items={this.state.intervenants}
                                    />
                                </div>
                            </div>
                            <hr />
                            <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                            </div>
 
                        </div>
                        </form>
                    </div>
                 </Fragment>)
            
}
}

