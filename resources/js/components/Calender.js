import React ,{Component,Fragment} from 'react';
import  'fullcalendar';
import  getCanvas from './html2canvas';
import InterventionCreation from './InterventionCreation'
import Model from './Model'
import OptionsBox from './OptionsBox'
import DatePicker from './DatePicker';


export default class Calender extends Component{
    state={
        data:[],
        dateRange:{
            start:{
                year:new Date().getFullYear(),
                month:new Date().getMonth()+1,
                day:1
            },
            end:{
                year:new Date(new Date().setDate(new Date().getDate()-1)).getFullYear(),
                month:new Date(new Date().setDate(new Date().getDate()-1)).getMonth()+1,
                day:new Date(new Date().getFullYear(),new Date(new Date().setDate(new Date().getDate()-1)).getMonth() + 1, 0).getDate()
            }
        },
        alreadyFetched:[{
            start:{
                year:new Date().getFullYear(),
                month:new Date().getMonth()+1,
                day:1
            },
            end:{
                year:new Date(new Date().setDate(new Date().getDate()-1)).getFullYear(),
                month:new Date(new Date().setDate(new Date().getDate()-1)).getMonth()+1,
                day:new Date(new Date().getFullYear(),new Date(new Date().setDate(new Date().getDate()-1)).getMonth() + 1, 0).getDate()
            }
        }],
        currentView:'month',
        showModel:false,
        showOptions:false,
        calenderCreated:false,
        clickedEvent:null,
        clickedEventPosition:null,
        navigateTo:''
    }

    componentDidMount(){
        this.fillCalender(this.state.dateRange,false)
    }


    fillCalender=(dateRange,exists)=>{
        
       // this.createCalender([]);
        let filterQueryStart=this.props.for.indexOf('?')==-1?'?':'&';

       let filterQuery=
       filterQueryStart+'s='+dateRange.start.year+'-'+dateRange.start.month+'-'+dateRange.start.day+
       '&e='+dateRange.end.year+'-'+dateRange.end.month+'-'+dateRange.end.day
            SpinnerStart()
        return  axios.get('/api/'+this.props.for+filterQuery)
            .then(({data})=>{
                let events=data.data.map((event)=>{
                    return {
                        id:event.id,
                        title:event[this.props.text],
                        start:new Date(event.date+' '+(event.period=='am'||event.period=='j'?'8:00':'13:00')),
                        end:new Date(event.date+' '+(event.period=='pm'||event.period=='j'?'18:00':'13:00')),
                        period:event.period,
                        date:event.date,
                        backgroundColor: this.getEventColor(event)
                  }

                });
                this.setState({data:events});
                if(!exists){
                    this.createCalender(this.state.data);
                  //  this.setState({calenderCreated:true})
                }
                SpinnerStop()
                this.setOnClickListeners();

            })
    }

    componentDidUpdate(){
        SpinnerStop();
    }
    createCalender=(events)=>{
    $('#calendar').fullCalendar({
            locale:'fr',
            firstDay:0,
            header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'month,agendaWeek,agendaDay'
            },
            contentHeight:'auto',
            contentWidh:'auto',
            buttonText: {
                today: 'aujourdhui',
                month: 'mois',
                week : 'semaine',
                day  : 'jour'
            },
            allDaySlot:false,
           // hiddenDays: [ 5, 6 ],
            slotDuration:'05:00:00',
            minTime:'08:00:00',
            maxTime:'18:00:00',
            events    : events,
            editable  : window.globalconf.user.is_admin,
            selectable: window.globalconf.user.is_admin,
            select:( start,end )=>{
                let selectedDate=this.getSelected(start,end)[0];
                
                this.setState({showModel:true,selectedDate:selectedDate.day,selectedPeriod:selectedDate.period})
            },
            eventDrop:(event)=>{
                if(!window.globalconf.user.is_admin) return; 
                
                SpinnerStart();            
                axios.put('/api/'+this.cleanedPath()+'/'+event.id, {
                    date:event.start._d.getFullYear()+'-'+(event.start._d.getMonth()+1)+'-'+event.start._d.getDate(),
                    period:this.getPeriod(event)
                }).then(res=>{
                    SpinnerStop();
                    toastr.success('changement effectuée avec succès')

                    this.refreshOnClickListeners()
                }).catch((err)=>{
                    SpinnerStop()
                    toastr.error(err)
                    this.props.refresh()
                });

            },
            eventClick:(event,jsEvent, view)=>{ 
                if(!window.globalconf.user.is_admin) return;          
               // rhistory.push('/'+this.props.for+'/'+event.id);
               this.setState({clickedEvent:event})
               this.setState({showOptions:true,selectedDate:event.date,selectedPeriod:event.period})

            },
            viewRender: (view) =>{   
                    if(!this.state.calenderCreated){
                        this.setState({calenderCreated:view.calendar})
                    if(this.props.startWith){    
                        this.setState({dateRange:this.props.startWith})
                        let {year,month,day}=this.props.startWith.start;
                        this.state.calenderCreated.gotoDate(year+'-'+month+'-'+day)
                        this.fetched(this.props.startWith)
                    }   
                }
                    this.renderView(view);      
             },
             eventRender:(event,element)=>{
                 element.html('<div class="fc-content"><span class="fc-time">'+event.period+' : </span> <span class="fc-title">'+event.title+'</span></div>')
             }
        })
        

   
   
    }
    //  TODO: method should be refactored as utility
    cleanedPath=()=>{
        let indexOfQuery=this.props.for.indexOf('?');
        if(indexOfQuery!=-1){
            return this.props.for.substr(0,indexOfQuery);
        }
        return this.props.for;
    }




    checkDateRangeChange=(old,current)=>{
        let edges=['start','end'];
        for(let edge in edges){
            for(let SubOld in old[edges[edge]]){
                for(let SubCurrent in current[edges[edge]]){
                    if(current[edges[edge]][SubCurrent] != old[edges[edge]][SubCurrent]){
                        return true;
                    }
                }
            }
        }    
        return false;
    }

    in_already_fetched_array(currentDateRange){
        for(let item in this.state.alreadyFetched){
            if(!this.checkDateRangeChange(this.state.alreadyFetched[item],currentDateRange)){
                return true;
            }
        }
        return false;
    }

    getPeriod=(event)=>{
        let startHour=event.start._d.getHours();
        let endHour=event.end._d.getHours();
        
        let startDay=event.start._d.getDate();
        let endDay=event.end._d.getDate();
      
        if(startHour==9 || startHour==1){
            if(endHour==14) return 'am'
            if(endHour==1) return 'j'
        }
     
        if(startHour==14 || (endHour==19 || endHour==1)){
             return 'pm'
        }
    
    }

    getSelected=(start,end)=>{
        let startDate=start._d.getDate();
        let endDate=end._d.getDate();
        let period=this.getPeriod({start,end})
        let selected=[];
        let dayRest=start._d.getFullYear()+'-'+(start._d.getMonth()+1)+'-';
        for(let i=startDate;
            ((i,period,endDate)=>{
                 if(endDate<startDate) return i<=startDate;
                 if(period=='j')   return  i<endDate;
                return i<=endDate;
            })(i,period,endDate);
            i++){
               
            if(endDate-startDate){
                if(period=='am' && i==endDate){
                    selected.push({
                        day:dayRest+i,
                        period:'am'
                    })
                }else
                if(period=='pm' && i==startDate){
                    selected.push({
                        day:dayRest+i,
                        period:'pm'
                    })
                }
                else
                if(period=='pm' && i==endDate){
                    selected.push({
                        day:dayRest+i,
                        period:'j'
                    })
                }
                else{
                    selected.push({
                        day:dayRest+i,
                        period:'j'
                    })
                }
            } else {
                selected.push({
                    day:dayRest+i,
                    period:period
                })
            }
        }
        return selected;
    }

    renderView=(view)=>{
    
        if(view.name!=this.state.currentView){
            view.calendar.removeEventSources()
            this.setState({currentView:view.name,alreadyFetched:[]})
        }
         let currentDateRange={
                        start:{
                            day:view.intervalStart._d.getDate(),
                            month:view.intervalStart._d.getMonth()+1,
                            year:view.intervalStart._d.getFullYear()
                        },
                        end:{
                            day:new Date(view.intervalEnd._d.setDate(view.intervalEnd._d.getDate()-1)).getDate(),
                            month:(new Date(view.intervalEnd._d.setDate(view.intervalEnd._d.getDate()-1)).getMonth())+1,
                            year:new Date(view.intervalEnd._d.setDate(view.intervalEnd._d.getDate()-1)).getFullYear()
                        }
         }

     let dateChanged=this.checkDateRangeChange(this.state.dateRange,currentDateRange);

         if(dateChanged &&  !this.in_already_fetched_array(currentDateRange) ) {
             SpinnerStart()

             this.fillCalender(currentDateRange,true)//TODO: change daterange
                    .then((res)=>{
                            view.calendar.addEventSource( this.state.data )
                            view.calendar.refetchEvents()
                        
                        SpinnerStop()
                        this.setState({dateRange:currentDateRange});

                        this.props.setParentDateRange(currentDateRange);
                       // if(!this.in_already_fetched_array(currentDateRange)){
                           this.fetched(currentDateRange);
                     //   }

                    })
               
         }  
    }
    closeModel=()=>{
        this.setState({showModel:false,clickedEvent:null})
    }
    closeOptionsBox=()=>{
        this.closeModel();
        this.setState({showOptions:false})
    }

    getEventColor=(event)=>{
        if(this.props.hasOwnProperty('colorPallet') && this.props.colorPallet.colors.hasOwnProperty(event[this.props.colorPallet.colorful]))
        return this.props.colorPallet.colors[event[this.props.colorPallet.colorful]];
        return '#2979ff';
    }

    export=()=>{
        getCanvas(document.querySelector("#calendar"),
                this.props.exportFileNameAppend+this.getcurrentViewName()
            )
     
    }
   
    getcurrentViewName=()=>{
        if(this.state.currentView=='month') return this.state.dateRange.start.month+'-'+this.state.dateRange.start.year;
        if(this.state.currentView=='agendaWeek') {
            if(this.state.dateRange.start.year==this.state.dateRange.end.year){
                if(this.state.dateRange.start.month==this.state.dateRange.end.month){
                    return this.state.dateRange.start.day+' au '+this.state.dateRange.end.day+' de '+this.state.dateRange.start.month+'-'+this.state.dateRange.start.year
                }
                return this.state.dateRange.start.day+'-'+this.state.dateRange.start.month
                +' au '+this.state.dateRange.end.day+'-'+this.state.dateRange.end.month+' de '+this.state.dateRange.start.year
            }
        }
    }

    setOnClickListeners=()=>{
        let domEvents=document.getElementsByClassName("fc-content");
        for(let event in domEvents){
           if(typeof(domEvents[event])=='object'){
                domEvents[event].addEventListener('click',this.getClickedEventPosition,false)
                }
        }

    }

    removeOnClickListeners=()=>{
        let domEvents=document.getElementsByClassName("fc-content");
        for(let event in domEvents){
           if(typeof(domEvents[event])=='object'){
                domEvents[event].removeEventListener('click',this.getClickedEventPosition,false)
          }
        }
    }

    refreshOnClickListeners=()=>{
        this.removeOnClickListeners()
        this.setOnClickListeners()
    }


    getClickedEventPosition=(e)=>{
        this.setState({clickedEventPosition:{x:e.clientX,y:e.clientY}})
    }

    deleteEntity=()=>{
        SpinnerStart()
        axios.delete('/api/'+this.cleanedPath()+'/'+this.state.clickedEvent.id)
        .then((res)=>{
            SpinnerStop()
            toastr.success("l'entité supprimée avec succès")
            this.props.refresh()
        
        })
        .catch((err)=>{
            SpinnerStop()
            toastErrors(err)
            this.props.refresh()
        });
    }

    fetched=(currentDateRange)=>{
        let alreadyFetched=this.state.alreadyFetched;
        alreadyFetched.unshift(currentDateRange)
        this.setState({alreadyFetched});
    }

    DatePickerOnChange=(e)=>{
        this.setState({navigateTo:e.currentTarget.value})
    }

    quickNavigation=()=>{
        if(this.state.calenderCreated){
            this.state.calenderCreated.gotoDate(this.state.navigateTo)
        }
    }


    render() {
        let OptionsBoxRef=null;

        return (
            <div className="animatedParent animateOnce">
                <div className="container-fluid p-0" >
                {this.state.showOptions &&
                <Model onClick={this.closeOptionsBox} position={this.state.clickedEventPosition} boxDimensions={{x:138,y:95}} >
                    <OptionsBox 
                        options={[
                            {icon:'icon-edit',text:'modifier',onClick:()=>{this.setState({showModel:true,showOptions:false}); }},
                            {icon:'icon-delete',text:'supprimer',onClick:()=>{this.deleteEntity(); this.setState({showOptions:false});}}
                        ]}
                    />
                </Model>}
                {this.state.showModel && this.props.hasOwnProperty('model') &&
                        (<Model onClick={this.closeModel}>
                            {this.props.model(this.state.selectedDate,this.state.selectedPeriod,this.state.clickedEvent)}
                        </Model>) 
                    }
                    <div className="row no-gutters">
                        <div className={this.props.hasOwnProperty('fullWidth')?"col-md-12":"col-md-10 offset-md-1"}>
                            <div className="card no-r no-b shadow">
                                <div className="card-body p-0">
                                    <button onClick={this.export} className="btn btn-primary btn-lg float-right">
                                    <i className="icon-save mr-2" />
                                    Exporter
                                    </button>
                                    <span className="float-left">
                                            <DatePicker onChange={this.DatePickerOnChange} name='navigation_date' format="Y-m" value={this.state.navigateTo}/>
                                    </span>        
                                    <button onClick={this.quickNavigation} className="btn btn-primary btn-md float-left ml-2">
                                        Navigation rapide
                                    </button>
                                    <div id="calendar"  />        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

