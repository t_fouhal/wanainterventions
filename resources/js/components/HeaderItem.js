import React from 'react';
import Dropdown from './Dropdown';


const HeaderItem=(props)=>{
    return (
        <li className="header-icon dib">
            <span>
                {props.hasOwnProperty('text')?props.text:null}
                {props.hasOwnProperty('icon') &&
                    (<i className={props.icon}></i>)
                }
            </span>
            {props.children}
        </li>

    );
}

export default HeaderItem;

