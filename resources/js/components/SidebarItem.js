import React  from 'react';

const SidebarItem=(props)=>(<li><a href={props.link}><i className={props.icon}></i> {props.title} </a></li>);


export default SidebarItem;

