import React from 'react';

const Footer=(props)=>{
    return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="footer">
                        <p>This dashboard was generated on <span id="date-time"></span> <a href="#" className="page-refresh">Refresh Dashboard</a></p>
                    </div>
                </div>
            </div>
    );
}

export default Footer;


