import React ,{Fragment} from 'react';

const Model=(props)=>{
    
    let x=props.hasOwnProperty('position') && props.position ?props.position.x:'25%'
    let y=props.hasOwnProperty('position') && props.position ?props.position.y:'30%'

    if(props.hasOwnProperty('position') && props.position){
       
        if(x>window.innerWidth-props.boxDimensions.x){
            x-=props.boxDimensions.x
        }
        if(y>window.innerHeight-props.boxDimensions.y){
            y-=props.boxDimensions.y
        }
        
    }
    
     return <Fragment>
                <div 
                onClick={()=>{props.onClick(); SpinnerStop()}}
                style={{backgroundColor:'rgba(255,255,255,0.6)',
                width:"100%",
                height:"100%",
                top:0,
                left:0,
                position:'fixed',
                zIndex:998
                }}></div>
                <div 
                    className="animated fadeInUpShort go"
                    style={{
                    zIndex:999,
                    position:'fixed',
                    width:'fit-content',
                    top:y,
                    left:x
                    
                }}>
                {props.children}
                </div>
            </Fragment>
}

export default Model;