import React  from 'react';


const Search=(props)=>{
    return (
        <div id="search">
            <button type="button" className="close">×</button>
            <form>
                <input type="search"  placeholder={props.placeholder} />
                <button type="submit" className="btn btn-primary">Search</button>
            </form>
        </div>
    );
}

export default Search;



