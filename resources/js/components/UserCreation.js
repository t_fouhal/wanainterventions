import React,{Component} from 'react';
import DatePicker from './DatePicker';


export default class UserCreation extends Component{
   
    state={}

    componentDidMount(){
        if(this.props.method=='POST') this.setState({is_admin:0})
        if(this.props.hasOwnProperty('data') && this.props.method=='PUT'){
            for(let field in this.props.data){
                let inputs=document.querySelectorAll('input[name='+field+']')
            inputs.forEach((input)=>{
                if(input){
                    if(field=='is_admin'){
                        if(input.id=='yes' && this.props.data[field]){
                            input.checked='true';
                        }
                        if(input.id=='no' && !this.props.data[field]){
                            input.checked='true';
                        }
                    }else if(field=='gender'){
                        if(input.value == this.props.data[field]){
                            input.checked='true';
                        } 
                    }
                    else input.value=this.props.data[field]
                }
            })  
                
            }           
            
        }
        SpinnerStop()
    }



    submit=(e)=>{
        e.preventDefault();
        SpinnerStart();
        if(this.props.method=='POST'){
            axios.post('/register',this.state)
            .then(res=>{
                SpinnerStop()
                toastr.success("l'utilisateur créé avec succés")
                rhistory.push('/users-list')})
                .catch(err=>{
                    SpinnerStop()
                    toastErrors(err)
                })
        }
        if(this.props.method=='PUT'){
            if(this.state.hasOwnProperty('is_admin')){
                this.setState({'is_admin':parseInt(this.state.is_admin)})
            }
            axios.put('/api/users/'+this.props.data.id,this.state)
            .then(res=>{
                SpinnerStop()
                toastr.success('changement effectuée avec success')})
                .catch(err=>{
                    toastErrors(err)
                    SpinnerStop()
                })
        }
    }
    onChange=(e)=>{
        if(e.target.name=='is_admin'){
            this.setState({[e.target.name]:parseInt(e.target.value)})

        }else
        this.setState({[e.target.name]:e.target.value})
    }

    render(){
        let defaultPrevilege=this.props.method=='POST'?{checked:true}:{}
            return (
                <div  className="col-md-7  offset-md-2">
                <form onSubmit={this.submit} autoComplete="off"
                onChange={this.onChange}>
                <div className="card no-b  no-r">
                    <div className="card-body">
                    <div className="form-row">
                        <div className="col-md-12">
     
                        <div className="form-group m-0">
                            <label htmlFor="name" className="col-form-label s-12">Nom complet</label>
                            <input id="name" name="name"  
                            className="form-control r-0 light s-12 " type="text" required/>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-6 m-0">
                            <label htmlFor="cnic" className="col-form-label s-12"><i className="icon-fingerprint" />Position</label>
                            <input id="cnic" name="position" className="form-control r-0 light s-12 date-picker" type="text" />
                            </div>
                            <div className="form-group col-6 m-0">
                            <label htmlFor="dob" className="col-form-label s-12"><i className="icon-calendar mr-2" />Date de naissance</label>
                                <DatePicker name="birth_date" onChange={(e)=>this.onChange(e)}/>       
                            </div>
                        </div>
                        <div className="form-group col-6 m-0">
                            <label htmlFor="gender" className="col-form-label s-12"></label>
                            <br />
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="male" name="gender" className="custom-control-input" value="male" required/>
                            <label className="custom-control-label m-0" htmlFor="male">mâle</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="female" name="gender" className="custom-control-input" value="female" required/>
                            <label className="custom-control-label m-0" htmlFor="female">Female</label>
                            </div>
                        </div>
                        <div className="form-group col-6 m-0">
                            <label htmlFor="is_admin" className="col-form-label s-12">Avec le privilège d'administrateur</label>
                            <br />
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="yes" name="is_admin" className="custom-control-input"  value={1}   />
                            <label className="custom-control-label m-0" htmlFor="yes">Oui</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="no" name="is_admin" className="custom-control-input" value={0}
                             {...defaultPrevilege}
                            />
                            <label className="custom-control-label m-0" htmlFor="no">Non</label>
                            </div>
                        </div>
                        </div>
            
                    </div>
                    <div className="form-row mt-1">
                        <div className="form-group col-6 m-0">
                        <label htmlFor="email" className="col-form-label s-12"><i className="icon-envelope-o mr-2" />Email</label>
                        <input id="email" name="email"  className="form-control r-0 light s-12 " type="text" required/>
                        </div>
                      
                        <div className="form-group col-6 m-0">
                        <label htmlFor="mobile" className="col-form-label s-12"><i className="icon-mobile-phone mr-2" />Mobile</label>
                        <input id="mobile" name="mobile"  className="form-control r-0 light s-12 " type="text" />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-8 m-0">
                        <label htmlFor="address" className="col-form-label s-12">Addresse</label>
                        <input type="text" name="address" className="form-control r-0 light s-12" id="address" />
                        </div>
                        <div className="form-group col-4 m-0">
                        <label htmlFor="password" className="col-form-label s-12">mot de passe</label>
                        <input type="password" name="password" className="form-control r-0 light s-12" id="password"  required={this.props.method=='POST'}/>
                        </div>
                    </div>
                  
                    </div>
                    <hr />
        
                    <div className="card-body">
                    <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                    </div>
                </div>
                </form>
            </div>
            );
}
}

