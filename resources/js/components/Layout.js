import React , {Fragment} from 'react';
import Header from './Header';
import Container from './Container';
import Search from "./Search";
import Sidebar from './Sidebar';
import { withRouter } from 'react-router-dom';

const Layout=({title,children,history})=>{
    window.rhistory=history;
   
    
    return (
        <Fragment>
            <Sidebar />
            <Header />
            <Container title={title}>
                {children}
            </Container>
        </Fragment>
    );
}

export default withRouter(Layout);

