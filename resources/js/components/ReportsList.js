import EditableList from './EditableList';


export default class ReportsList extends EditableList{
    getPaths=()=>{
        return {
           path:this.props.path,
           entityPath:this.props.entityPath
        }
    }
   
    defineObject=(item)=>{
       return {
           id:item.id,
           status:item.destination?{}:{text:'prochain rapport',color:'info'},
           time:item.date
       }
    }
   

   

}

