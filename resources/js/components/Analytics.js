import React ,{Component} from 'react';
import DatePicker from './DatePicker';
import Select2 from './Select2';
import InfoBox from './InfoBox';
import BarsChart from './BarsChart';
import AnalyticsList from './AnalyticsList';


let filterMap={
    'interventions':[
        {key:'clients',value:'client_id'},
        {key:'intervenants',value:'user_id'},
        {key:'status',value:'status'}
    ],
    'journees':[
        {key:'clients',value:'client_id'},
        {key:'intervenants',value:'user_id'},
    ]

}

let columnTableMap={
    client_id:'clients',
    user_id:'intervenants',
    status:'interventions'
}
export default class Analytics extends Component{

    state={
        e2Items:[],
        specificSelectText:null,
        specificSelectItems:[],
        data:{mode:'list'},
        infoBox:null,
        aggregation:null,
        level4:null
    }

    submit=(e)=>{
        e.preventDefault();
        if(!this.state.data.hasOwnProperty('e1') || !this.state.data.hasOwnProperty('e2')
            || (this.state.data.hasOwnProperty('e1') && !this.state.data.e1)
            ||  (this.state.data.hasOwnProperty('e2') && !this.state.data.e2)  ) {
            toastr.error('vous devez choisir les deux entitées')
            return;
        }
        if(!this.state.data.hasOwnProperty('specific') 
            || (this.state.data.hasOwnProperty('specific') 
            && !this.state.data.specific)){
            this.getChartData();
        }else{
            this.getInfoBoxData()
        }
    }


    onChange=(e)=>{
        if(e.target.name!='mode') this.setState({aggregation:null,infoBox:null})
        if(e.target.name=='e1'){
            this.handleE1Change(e)
        }
  
        let newObject={}
        for(let property in this.state.data){
            newObject[property]=this.state.data[property]
        }
        newObject[e.target.name]=e.target.value;
        this.setState({data:newObject})
    }

    makeSelectItems=(data)=>{
         return data.data.map((item)=>{
             return {key:item.name,value:item.id}
            })
    }

    getChartData=()=>{
        this.setState({aggregation:null,infoBox:null})
        let dateRangeQuery=this.getDateRangeQuery()
        SpinnerStart();
        axios.get('/api/'+this.state.data.e1+'/count?'+this.state.data.e2+dateRangeQuery)
        .then(({data})=>{
            this.setState({aggregation:data})
            SpinnerStop();
        })
    }

   

   getDateRangeQuery=()=>{
        let dateRangeQuery='';
        let edges=['from','to'];
        for(let edge in edges){
            if(this.state.data.hasOwnProperty(edges[edge]) && this.state.data[edges[edge]]){
                dateRangeQuery='&'+edges[edge]+'='+this.state.data[edges[edge]];
            }
        }
        return dateRangeQuery;
   } 
   handleE1Change=(e)=>{
            this.setState({e2Items:filterMap[e.target.value]});
            if(e.target.value=='interventions'){
                filterMap['user_id']=[{key:'nouvelle',value:0}, //TODO:set all statuses to tables and make them generic
                                        {key:'completée',value:1},
                                        {key:'reportée',value:2}]
            }
            if(e.target.value=='occupations'){
                filterMap['user_id']=[[
                                    {key:'WANA',value:0},
                                    {key:'maladie',value:-1},
                                    {key:'absence non autorisée',value:-2},
                                    {key:'absence autorisée',value:-3},
                                    {key:'congé',value:-4},
                                    {key:'formation',value:-5}
                                ]]
            }
   }

 

   dateRangeSringTitle=()=>{
       let dateString=''
       if(this.state.data.hasOwnProperty('from') && this.state.data.from){
        if(!this.state.data.hasOwnProperty('to') || !this.state.data.to){
            dateString+=" depuis "+this.state.data.from
        }else 
            dateString+="du "+this.state.data.from
       }
       if(this.state.data.hasOwnProperty('to') && this.state.data.to){
            dateString+=" au "+this.state.data.to
        }
       return dateString;
   }


    render(){
        
        return (
            <div className="col-md-12">
            <form onSubmit={this.submit}
                onChange={this.onChange}
                onSelect={this.onChange}
                autocomplete="off"
                >
            <div className="card no-b  no-r">
                <div className="card-body">
                <div className="form-row">
                    <div className="col-md-12">

                    <div className="form-row">
                        <div className="form-group col-5 m-0">
                        <label htmlFor="e1" className="col-form-label s-12">Entité 1</label>
                        <Select2 
                                        onChange={(e)=>this.onChange(e)}
                                        name='e1'
                                        id='e1'
                                        selected={[]}
                                        items={[
                                            {key:'Interventions',value:'interventions'},
                                            {key:'nombre de journées',value:'journees'},
                                        ]}
                         />
                        </div>
                       
                        <div className="form-group col-5 m-0">
                        <label htmlFor="e2" className="col-form-label s-12"> Entité 2</label>
                        <Select2 
                                        onChange={(e)=>this.onChange(e)}
                                        name='e2'
                                        id='e2'
                                        selected={[]}
                                        items={this.state.e2Items}
                                    />
                        </div>
                        <div className="form-group col-2 m-0">
                            <label htmlFor="mode" className="col-form-label s-12">mode</label>
                            <br />
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="list" name="mode" className="custom-control-input"  value="list" {...(this.state.data.mode=='list'?{checked:true}:{})}/>
                            <label className="custom-control-label m-0" htmlFor="list">Liste</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="bars" name="mode" className="custom-control-input" value="bars" {...(this.state.data.mode=='bars'?{checked:true}:{})}/>
                            <label className="custom-control-label m-0" htmlFor="bars">Bars</label>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
                <div className="form-row mt-1">
                    <div className="form-group col-6 m-0">
                         <label htmlFor="from" className="col-form-label s-12">début</label>
                        <DatePicker name='from' onChange={this.onChange}/>
                    </div>
                    <div className="form-group col-6 m-0">
                         <label htmlFor="to" className="col-form-label s-12">fin</label>
                        <DatePicker name='to' onChange={this.onChange}/>
                    </div>
                </div>
                <div className="form-row mt-1">
                <div className="col-12">
                    {this.state.data.mode=='bars' && this.state.infoBox&&
                            <InfoBox 
                            {...this.state.infoBox}
                            />
                            }
                            {this.state.data.mode=='bars' && this.state.aggregation&&
                            <BarsChart 
                                data={this.state.aggregation}
                                title={this.state.data.e1}
                                subTitle={'par '+columnTableMap[this.state.data.e2]+' '+this.dateRangeSringTitle()}
                            />
                            }
                        {this.state.data.mode=='list' && this.state.aggregation &&
                            <AnalyticsList data={this.state.aggregation} for={columnTableMap[this.state.data.e2]}/>
                        }    
                    </div>
                </div>

            
                </div>
                <hr />
                <div className="card-body">
                        <button type="submit" className="btn btn-primary btn-lg"><i className="icon-calculator mr-2" />Calculer</button>
                 </div>

            </div>
            </form>
        </div>
        );
    }
}