import React ,{Component} from 'react';
import DatePicker from './DatePicker';
import Select2 from './Select2';

export default class Occupation extends Component{

    state={
        intervenants:[],
        selectedStatus:[],
        selectedIntervenant:[]
    }

    componentDidMount(){
        if(this.props.hasOwnProperty('as_model')){
        this.setState({data:
            {date:this.props.selectedDate,
                period:this.props.selectedPeriod[0],user_id:this.props.user},
                selectedPeriod:this.props.selectedPeriod,
                selectedIntervenant:[this.props.user]
            })
        }
        SpinnerStart()
        axios.get('/api/intervenants')
        .then(({data}) =>{
            let formattedIntervenants=[]; 
                for(let intervenant in data.data){
                    formattedIntervenants.push({
                        key:data.data[intervenant].name,
                        value:data.data[intervenant].id
                    })
                }
            this.setState({intervenants:formattedIntervenants})
            SpinnerStop()
            }).then(res=>this.handlePutMethod());
    }
    onChange=(e)=>{
        let data={};
        for(let input in this.state.data){
            data[input]=this.state.data[input];
        }
        data[e.target.name]=e.target.value;
        this.setState({data})
    }
    submit=(e)=>{
        e.preventDefault()
        SpinnerStart()
        let method=this.props.method.toLowerCase();
        axios[method]('/api/occupations'+(method=='put'?'/'+this.props.id:''),this.state.data)
        .then((res)=>{
                SpinnerStop()
                toastr.success('l\'occupation '+(method=='put'?'modifiée':'crée')+' avec succés')
                if(this.props.hasOwnProperty('as_model')) this.props.refreshCalender()
        }).catch((err)=>{
            SpinnerStop()
            toastErrors(err)
        })
    }

    handlePutMethod=()=>{
        if(this.props.method=='PUT'){
            SpinnerStart()
            axios.get('/api/occupations/'+this.props.id)
                .then(({data})=>{
                  this.setState({selectedStatus:[data.data.status],selectedIntervenant:[data.data.user_id],selectedPeriod:[data.data.period]})
                  document.getElementById('date').value=data.data.date
                  SpinnerStop()
                })
        }
    }


    render(){
        return (
            <div className={this.props.hasOwnProperty('as_model')?"col-md-12":"col-md-7  offset-md-2"}>
                        <form onSubmit={this.submit}
                            onChange={this.onChange}
                            onSelect={this.onChange}
                            autocomplete="off"
                            >
                        <div className="card no-b  no-r">
                            <div className="card-body" style={this.props.hasOwnProperty('as_model')?{border:'2px solid #bdc3c7'}:{}}>
                            <div className="form-row">
                                <div className="col-md-12">
            
                                <div className="form-row">
                                    <div className="form-group col-5 m-0">
                                    <label htmlFor="cnic" className="col-form-label s-12"><i className="icon-fingerprint" />TS/Ingénieur</label>
                                        <Select2 
                                            onChange={this.onChange}
                                            name='user_id'
                                            id='userSelect2'
                                            selected={this.state.selectedIntervenant}
                                            items={this.state.intervenants}
                                        />
                                    </div>
                                    <div className="form-group col-4 m-0">
                                    <label htmlFor="date" className="col-form-label s-12"><i className="icon-calendar mr-2" />La date</label>
                                        <DatePicker onChange={this.onChange} name="date" value={this.props.selectedDate}/>
                                    </div>
                                    <div className="form-group col-3 m-0">
                                     <label htmlFor="mobile" className="col-form-label s-12"><i className="icon-user-group mr-2" />La periode</label>
                                            <Select2 
                                                onChange={this.onChange}
                                                name='period'
                                                id='periodSelect2'
                                                selected={[this.state.selectedPeriod]}
                                                items={[
                                                    {key:'avant midi',value:'am'},
                                                    {key:'aprés midi',value:'pm'},
                                                    {key:'journée',value:'j'},                
                                                ]}
                                            />
                                    </div>
                                </div>
                            </div>

                            </div>
                            <div className="form-row mt-1">
                                <div className="form-group col-7 m-0">
                                <label htmlFor="mobile" className="col-form-label s-12"><i className="icon-user-group mr-2" />Status</label>
                                    <Select2 
                                        onChange={(e)=>this.onChange(e)}
                                        name='status'
                                        id='statusSelect2'
                                        selected={this.state.selectedStatus}
                                        items={[
                                            {key:'WANA',value:0},
                                            {key:'maladie',value:-1},
                                            {key:'absence non autorisée',value:-2},
                                            {key:'absence autorisée',value:-3},
                                            {key:'congé',value:-4},
                                            {key:'formation',value:-5}
                                        ]}
                                    />
                                </div>
                              
                                <div className="form-group col-5 m-0">
                                <div className="card-body">
                                    <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                                </div>
                                </div>
                            </div>
                        
                            </div>
                            <hr />
                

                        </div>
                        </form>
                    </div>
        
            );
    }
}

