import React ,{Fragment} from 'react';
import { Link } from 'react-router-dom';



const SidebarItemsGroup=(props)=>{
    return (
        <Fragment>
            <li className="treeview"><a href="#">
                {props.hasOwnProperty('icon')&&(<i className={props.icon+" s-18"} />)}
                <span>{props.title}</span>
                <i className="icon icon-angle-left s-18 pull-right" />
            </a>
                <ul className="treeview-menu">
                    {props.items.map((item,key)=>
                        (<li key={key}>
                            <Link to={item.hasOwnProperty('link')?item.link:'#'} >
                                {item.hasOwnProperty('icon')&&(<i className={item.icon} />)}
                                {item.title}
                            </Link>
                        </li>))}
                </ul>
            </li>
        </Fragment>
    );
}

export default SidebarItemsGroup;



