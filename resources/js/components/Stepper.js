import React  , {Component} from 'react';
import 'smartwizard';


export default class Stepper extends Component{
   /* state={
        lastMove:{}
    }*/

    componentDidMount=()=>{
        var element = $("#stepper");
        // assigning stepper options
        var options={transitionEffect: "fade",lang:{next:'Suivant',previous:'Précedent'}};
        if(this.props.hasOwnProperty('options'))
        {
            for(let option in this.props.options)
            options[option]=this.props.options[option];
        }
            element.smartWizard(options);

        //declaring events
            element.on('showStep',(e,object,number,direction)=>{
              //  this.setState({lastMove:{number,direction,hash:rhistory.location.hash}})
                if(direction=='forward'){
                    for(let item of this.props.items){
                        if(rhistory.location.hash == '#'+item.hashLink ){
                            item.action()
                                .then((res)=>res.json())
                                .then(()=>{
                                    SpinnerStop()
                                })
                                .catch((err)=>{
                                    SpinnerStop();
                                    rhistory.goBack()
                                    toastr.error('erreur')
                                })
                        }
                    }
                }

            })
            element.on('leaveStep',()=>{
                SpinnerStop();
            })

       SpinnerStop();
    }




    render() {
        return (
                <div id="stepper" className="stepper sw-main sw-theme-default" >
                    <ul className="nav step-anchor nav-tabs">
                        {this.props.items.map((item,key)=>
                            <li className={key==0?"nav-item active":"nav-item"} key={key}>
                                <a href={"#"+item.hashLink} className="nav-link">
                                    {item.step}<br />
                                    {item.hasOwnProperty('description')&&
                                        <small>{item.description}</small>
                                    }
                                </a>
                            </li>
                        )}
                    </ul>
                    <div className="card no-b shadow sw-container tab-content" >
                        {this.props.items.map((item,key)=>{
                            return (<div id={item.hashLink} className="card-body text-center p-5 tab-pane step-content"
                                         style={{display: key==0?'block':''}}  key={key}>
                                        {item.vue}
                                    </div>)
                        })}
                        <div className="btn-toolbar sw-toolbar sw-toolbar-bottom justify-content-end">
                            <div className="btn-group mr-2 sw-btn-group" role="group">
                                <button className="btn btn-secondary sw-btn-prev disabled" type="button">Précedent</button>
                                <button className="btn btn-secondary sw-btn-next" type="button">Suivant</button>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

