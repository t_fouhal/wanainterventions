import React ,{Fragment,Component} from 'react';
import Pagination from './Pagination';
import { Link } from 'react-router-dom';


export default class List extends Component{
    componentDidMount(){
        SpinnerStop()
    }

    componentDidUpdate(){
        SpinnerStop()
    }

    delete=(e)=>{
        SpinnerStart()
        e.preventDefault();
        
        axios.delete('/api'+this.cleanedPath()+'/'+e.target.id)
        .then(res=>{
            if(this.props.hasOwnProperty('deleted')){
                this.props.deleted()
            }
            
            toastr.success("l'élément supprimé avec succée")
            SpinnerStop()
        }).catch(err=>{
            toastr.error(err)
            SpinnerStop()
        })
    }
//TODO: need to be refactored
    cleanedPath(){
        let indexOfQuery=this.props.entityPath.indexOf('?');
        if(indexOfQuery!=-1){
            return this.props.entityPath.substr(0,indexOfQuery);
        }
        return this.props.entityPath;
    }



render(){
    let onEdit=this.props.hasOwnProperty('onEdit')?this.props.onEdit:()=>{}
    let cleanedPath=this.cleanedPath();
        return <Fragment> 
            {this.props.items.map.length &&
                    <div className="col-md-12">
                                <div className="card no-b shadow">
                                    <div className="card-body p-0">
                                        <div className="table-responsive">
                                            <table className="table table-hover ">
                                                <tbody>
                                                {this.props.items.map((item,key)=>
                                                    <tr className={key==0?"no-b":''} key={key} >
                                                        <td>
                                                            <h6>{item.text}</h6>
                                                            <small className="text-muted">{item.subText}</small>
                                                        </td>
                                                        <td>{item.info}</td>
                                                        <td>
                                                            {item.hasOwnProperty('status')&&
                                                            <span className={"badge badge-"+item.status.color}>{item.status.text}</span>
                                                            }
                                                        </td>
                                                        <td>
                                                            {item.hasOwnProperty('time') &&
                                                            <span><i className="icon icon-timer"/> {item.time}</span>
                                                            }
                                                        </td>
                                                        {window.globalconf.user.is_admin &&
                                                        <td>
                                                            <Link to={cleanedPath+'/'+item.id} onClick={onEdit} id={item.id}>
                                                            <i  className="icon icon-pencil"/></Link>
                                                             <a href="#"  onClick={this.delete} id={item.id} >
                                                             <i className="icon icon-close2 text-danger-o text-danger ml-5" id={item.id}/></a>
                                                        </td>
                                                        }
                                                    </tr>
                                                )}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                {this.props.hasOwnProperty('pagination') && this.props.pagination && this.props.pagination.last_page>1 &&
                                    <Pagination pagination={this.props.pagination}  
                                     path={this.props.path} />
                                }
                   </div>
            }
                </Fragment>
}
}

