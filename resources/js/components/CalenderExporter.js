import React ,{Component} from 'react'
import DatePicker from './DatePicker'
import Select2 from './Select2'

export default class  CalenderExporter extends Component{
  
    render(){
            return <div className={"col-md-12 offset-md-2"}>
                        <form 
                            method='POST'
                            action={window.globalconf.applicationpath+'/api/occupations/export'}
                            target='_blank'
                            >
                            <div className="card no-b  no-r" >
                                <div className="card-body" style={{border:'2px solid #bdc3c7'}}>
                                <div className="form-row">
                                    <div className="col-md-12">

                                    <div className="form-row">
                                    
                                        <div className="form-group col-5 m-0">
                                            <label htmlFor="start" className="col-form-label s-12"><i className="icon-time mr-2" />debut</label>
                                            <DatePicker name='start' onChange={this.onChange}/>
                                        </div>
                                        <div className="form-group col-5 m-0">
                                            <label htmlFor="end" className="col-form-label s-12"><i className="icon-time mr-2" />fin</label>
                                            <DatePicker name='end' onChange={this.onChange}/>
                                        </div>
                                        <div className="form-group col-2 m-0">
                                            <label htmlFor="formatSelect2" className="col-form-label s-12"><i className="icon-time mr-2" />Format</label>
                                            <Select2
                                             name='format'
                                             id='formatSelect2'
                                             onChange={()=>{}}
                                             items={[
                                                 {key:"Xls",value:"Xls"},
                                                 {key:"Xlsx",value:"Xlsx"},
                                                 {key:"Ods",value:"Ods"},
                                                 {key:"Csv",value:"Csv"},
                                                 {key:"Html",value:"Html"}
                                             ]}
                                             selected={["Xlsx"]}
                                             />
                                        </div>
                                    </div>
                                </div>

                                </div>  
                                <hr />
                                <div className="card-body">
                                        <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Exporter</button>
                                </div>  
                                </div>
                            

                            </div>
                        </form>
                    </div>
    }
}
