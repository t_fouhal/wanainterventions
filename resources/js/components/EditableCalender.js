import React ,{Component} from 'react';
import Calender from './Calender';

export default class EditableCalender extends Component{
    state={
        calenderKey:1,
        selectedPeriod:null,
        selectedDate:null,
        currentDateRange:false
    }

    
    refreshCalender=()=>{
        this.setState({calenderKey:this.state.calenderKey+1})   
    }

    setCurrentDateRange=(date)=>{
        this.setState({currentDateRange:date})
    }

    model=(selectedDate,selectedPeriod,clickedEvent)=>{
        let click=clickedEvent?{id:clickedEvent.id}:{}

        let props={
           method:clickedEvent?"PUT":"POST",
           as_model:true,
           selectedDate:selectedDate,
           selectedPeriod:[selectedPeriod],
           refreshCalender:this.refreshCalender
       }
       return  this.view(props,click);
    }
    exportFileNameAppend=()=>{
        return this.props.hasOwnProperty('fileAppendName')?this.props.fileAppendName+' ':'';
    }


    calenderEntity=(Entity,viewParams={})=>{
        return <Calender 

                startWith={this.state.currentDateRange}
                setParentDateRange={this.setCurrentDateRange}

                refresh={this.refreshCalender}
                {...viewParams}
                exportFileNameAppend={this.exportFileNameAppend()}
                key={this.state.calenderKey}
                for={Entity.for} 
                text={Entity.text} 
                colorPallet={Entity.colorPallet}
                model={this.model}
                />
    }

   
}


