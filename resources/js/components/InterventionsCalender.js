import React  from 'react';
import EditableCalender from './EditableCalender';
import InterventionCreation from './InterventionCreation';




export default class InterventionsCalender extends EditableCalender{
   
   
view=(props,click)=>{
    return <InterventionCreation {...props} {...click} />
}



render(){
    return this.calenderEntity({
        for:this.props.for,
        text:this.props.hasOwnProperty('text')?this.props.text:'client_name',
        colorPallet:{
            colorful:'status',
            colors:{
                0:'#3498db',
                1:'#1abc9c',
                2:'#e74c3c'
        }}
    })
}
   
}