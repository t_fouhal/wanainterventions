import React ,{Component} from 'react';
import Select2 from './Select2';


export default class Client extends Component{
    
    state={
        contracts:[],
        selectedContract:[]
    }

    componentDidMount(){
        SpinnerStart();
        axios.get('/api/contracts')
        .then(({data}) =>{
            let formattedContracts=[]; 
                for(let contract in data.data){
                    formattedContracts.push({
                        key:data.data[contract].name,
                        value:data.data[contract].id
                    })
                }
            this.setState({contracts:formattedContracts})
            if(this.props.method=='POST') SpinnerStop()
            }).then(res=>{
                this.handlePutMethod()
            });

    }

    submit=(e)=>{
        e.preventDefault()
        SpinnerStart()
        let method=this.props.method.toLowerCase();
        axios[method]('/api/clients'+(method=='put'?'/'+this.props.data.id:''),this.state.data)
        .then((res)=>{
                SpinnerStop()
                toastr.success('le client '+(method=='put'?'modifiée':'crée')+' avec succés')
        }).catch((err)=>{
            SpinnerStop()
            toastErrors(err)
        })
    }

    onChange=(e)=>{
        let data={};
        for(let input in this.state.data){
            data[input]=this.state.data[input];
        }
        data[e.target.name]=e.target.value;
        this.setState({data})
    }

    handlePutMethod=()=>{
        if(this.props.method=='PUT'){
        this.setState({selectedContract:[this.props.data.contract_id]})
        document.getElementById('description').value=this.props.data.description;
        document.getElementById('name').value=this.props.data.name;
        SpinnerStop()
       }
    }

    render(){
        return (
            <div className="col-md-7  offset-md-2">
                <form onSubmit={this.submit}
                    onChange={this.onChange}
                    onSelect={this.onChange}
                    autocomplete="off"
                    >
                    <div className="card no-b  no-r">
                        <div className="card-body">
                        <div className="form-row">
                            <div className="col-md-12">

                            <div className="form-row">
                                <div className="form-group col-12 m-0">
                                <label htmlFor="name" className="col-form-label s-12">Nom d'entreprise</label>
                                    <input id="name" name="name"  
                                    className="form-control r-0 light s-12 " type="text" required/>
                                </div>
                            
                            </div>
                        </div>

                        </div>
                        <div className="form-row mt-1">
                            <div className="form-group col-12 m-0">
                            <label htmlFor="mobile" className="col-form-label s-12"><i className="icon-user-group mr-2" />description</label>
                            <textarea className="form-control r-0" id="exampleFormControlTextarea2" rows={3} defaultValue={""} name="description"  id='description'/>
                            </div>
                        </div>
                    
                        </div>
                        <hr />
                        <div className="card-body">
                                <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                        </div>

                    </div>
                </form>
            </div>


        );
    }
   
}

