import React ,{Component,Fragment} from 'react';
import Select2 from './Select2';
import DatePicker from './DatePicker';
import Model from './Model';
import Report from './Report';
import ReportsList from './ReportsList';


export default class Contract extends Component{
    
    state={
        contracts:[],
        selectedFrequency:[],
        selectedClient:[],
        selectedBIntervenant:[],
        selectedPIntervenant:[],
        selectedClient:[],
        clients:[],
        intervenants:[],
        showModel:false,
        listKey:1,
        reportModelMethod:null,
        reportProps:{}
    }

    componentDidMount(){
        return axios.all([(()=>axios.get('/api/clients'))(),
        (()=>axios.get('/api/intervenants'))()])
            .then(axios.spread((clients, intervenants) =>{
            let formattedClients=[],formattedIntervenants=[]; 
                for(let client in clients.data.data){
                    formattedClients.push({
                        key:clients.data.data[client].name,
                        value:clients.data.data[client].id
                    })
                }
                for(let intervenant in intervenants.data.data){
                    formattedIntervenants.push({
                        key:intervenants.data.data[intervenant].name,
                        value:intervenants.data.data[intervenant].id
                    })
                }
            this.setState({clients:formattedClients,intervenants:formattedIntervenants})
            SpinnerStop()
            }))
            .then((res)=>this.handlePutMethod());
    
       
    }

    submit=(e)=>{
        e.preventDefault()
        SpinnerStart()
        let method=this.props.method.toLowerCase();
        axios[method]('/api/contracts'+(method=='put'?'/'+this.props.id:''),this.state.data)
        .then((res)=>{
                SpinnerStop()
                toastr.success('la contrat '+(method=='put'?'modifiée':'crée')+' avec succés')
        }).catch((err)=>{
            SpinnerStop()
            toastErrors(err)
        })
    }


    onChange=(e)=>{
        let data={};
        for(let input in this.state.data){
            data[input]=this.state.data[input];
        }
        data[e.target.name]=e.target.value;
        this.setState({data})
    }

    handlePutMethod=()=>{
        if(this.props.method=='PUT'){
            SpinnerStart()
            axios.get('/api/contracts/'+this.props.id)
            .then(({data})=>{
                this.setState({selectedFrequency:[data.data.frequency],
                                selectedClient:[data.data.client_id],
                                selectedPIntervenant:[data.data.principal_intervenant],
                                selectedBIntervenant:[data.data.backup_intervenant]})
               let inputs=['days_nbr','start','end'];
                for(let input in inputs){
                    document.getElementById(inputs[input]).value=data.data[inputs[input]];
                }

                SpinnerStop()
            })
       }
    }

    refresh=()=>{
        this.setState({showModel:false,listKey:this.state.listKey+1});
    }

    onEditReport=(e)=>{
        e.preventDefault()
        this.setState({showModel:true,reportModelMethod:'PUT',reportProps:{id:e.currentTarget.id}})
    }

    showPostModel=()=>{
        this.setState({showModel:true,reportModelMethod:'POST',reportProps:{contract:this.props.id}})
    }
    showPutModel=(e)=>{
        
    }
    render(){
        return (
            <Fragment>
            <div className="col-md-7  offset-md-2">
            <form onSubmit={this.submit}
                onChange={this.onChange}
                onSelect={this.onChange}
                autocomplete="off"
                >
            <div className="card no-b  no-r">
                <div className="card-body">
                <div className="form-row">
                    <div className="col-md-12">

                    <div className="form-row">
                        <div className="form-group col-5 m-0">
                        <label htmlFor="clientSelect2" className="col-form-label s-12">le Client</label>
                        <Select2 
                                onChange={this.onChange}
                                name='client_id'
                                id='clientSelect2'
                                selected={this.state.selectedClient}
                                items={this.state.clients}
                        />
                        </div>
                        <div className="form-group col-4 m-0">
                        <label htmlFor="days_nbr" className="col-form-label s-12">Nbr de journées</label>
                            <input id="days_nbr" name="days_nbr"  
                            className="form-control r-0 light s-12 " type="decimal" required/>
                        </div>
                        <div className="form-group col-3 m-0">
                        <label htmlFor="frequencySelect2" className="col-form-label s-12"><i className="icon icon-schedule" />Frequence</label>
                        <Select2 
                                onChange={this.onChange}
                                name='frequency'
                                id='frequencySelect2'
                                selected={this.state.selectedFrequency}
                                items={[
                                    {key:'par mois',value:'m'},
                                    {key:'par semaine',value:'w'},
                                    {key:'par jour',value:'d'},
                                    {key:'par année',value:'y'}
                                ]}
                        />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-6 m-0">
                        <label htmlFor="principalSelect2" className="col-form-label s-12">Intervenant Principal</label>
                        <Select2 
                                onChange={this.onChange}
                                name='principal_intervenant'
                                id='principalSelect2'
                                selected={this.state.selectedPIntervenant}
                                items={this.state.intervenants}
                        />
                        </div>       
                        <div className="form-group col-6 m-0">
                        <label htmlFor="backupSelect2" className="col-form-label s-12">Intervenant Backup</label>
                        <Select2 
                                onChange={this.onChange}
                                name='backup_intervenant'
                                id='backupSelect2'
                                selected={this.state.selectedBIntervenant}
                                items={this.state.intervenants}
                        />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-6 m-0">
                            <label htmlFor="start" className="col-form-label s-12">début</label>
                            <DatePicker name='start' onChange={this.onChange}/>
                        </div>
                        <div className="form-group col-6 m-0">
                            <label htmlFor="end" className="col-form-label s-12">fin</label>
                            <DatePicker name='end' onChange={this.onChange}/>
                        </div>
                    </div>    
                </div>

                </div>
                
                </div>
                <hr />
                <div className="card-body">
                        <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                 </div>

            </div>
            </form>
            </div>
            {this.props.method=='PUT' &&
            <div className="row my-3">
            {this.state.showModel && this.state.reportModelMethod &&
            <Model onClick={()=>this.setState({showModel:false})}>
                    <Report as_model method={this.state.reportModelMethod} 
                    refreshContract={this.refresh} 
                    {...this.state.reportProps}/>
            </Model>
            }
            <div className="col-md-7 offset-md-2">
                <div className="card">
                    <div className="card-header white">
                        <i className="icon icon-add_circle float-right text-green" style={{cursor:'pointer'}} onClick={this.showPostModel}/>
                        <h6 class="mb-3">Les rapports :</h6> 
                        {<ReportsList  key={this.state.listKey}
                            path={'/contracts/'+this.props.id}
                            entityPath={'/reports?contract_id='+this.props.id}
                            buttonsListeners={{
                                onEdit:this.onEditReport
                            }}
                        />}
                    </div>
                    
                </div>
            </div>
            </div>
            }
           

            </Fragment>                    



        );
    }
   
}

