import React  from 'react';


const InfoBox=(props)=>{
return <div className="p-5 light " style={{width: '257.5px', margin: '15px auto 0 auto'}} >
                            <h5 className="font-weight-normal s-14">{props.topText}</h5>
                            <span className="s-48 font-weight-lighter text-primary">{props.topValue}</span>
                            <div> {props.bottomText}
                                <span className="text-primary ml-2">
                                    <i 
                                    className={props.hasOwnProperty('status')?
                                    "icon icon-arrow_"+props.status+"ward":""} />
                                    {props.bottomValue}
                                    </span>
                            </div>
                        </div>}

export default InfoBox;
