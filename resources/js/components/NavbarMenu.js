import React from 'react';
const NavbarMenu=(props)=><div className="navbar-custom-menu p-t-10">
                        <ul className="nav navbar-nav">
                            {props.children}
                        </ul>
                      </div> 
export default NavbarMenu;                       