window.$ = window.jQuery = require ('jquery');
import 'jquery-ui-dist/jquery-ui';

import '../core/_preloader';
import  '../libs/modernizr';
import  '../libs/jquery.easing';

import  '../core/_popper';
import  'bootstrap';
import  '../libs/jquery.waitforimages';
import  '../libs/css3-animate-it';
import  '../core/_sticky';
import  '../functions';
import  '../scripts';
import  '../core/masonary';
import  '../core/_sidebar';
import  '../core/_counter';
import  '../core/_countDown';
import  '../core/charts';
import  '../core/_knob';
import  '../core/_map';
import  '../core/_promotionsBar';


window.SpinnerStart = function(size){
    document.getElementById('ploader').classList.value='preloader-wrapper '+size+' active';
    document.getElementById('backdrop').classList='backdrop'
};

window.SpinnerStop = function(){
    document.getElementById('ploader').classList.value='preloader-wrapper';
    document.getElementById('backdrop').classList=''
};


window.toastErrors=function(err){
   /* if(err.hasOwnProperty("message")){
        toastr.err(err.message)
    }
    else*/
    for(let error in err.response.data.errors){
        for(let suberror in err.response.data.errors[error]){
            toastr.error(err.response.data.errors[error][suberror])
        }
    }
}

window.getInterventionStatusListObject=(intervention)=>{
            switch (intervention.status){
                case 0: return {text:'nouvelle',color:'info'};
                case 1: return {text:'completée',color:'success'};
                case 2: return {text:'reportée',color:'danger'}; 
            }
}


window.getFullPeriodText=(key)=>{
    switch(key){
        case 'am': return 'demi journée (am)';
        case 'pm': return 'demi journée (pm)';
        case 'j': return 'journée';
    }
}


window.concateIntervenants=(intervention)=>{
    let subText=intervention.intervenants.length?'avec':'';
    for(let intervenant in intervention.intervenants){
        subText+=' '+intervention.intervenants[intervenant].name+',';
    }
    return subText;
}

window.getInterventionObjectsArray=(data)=>{
    let interventions=[];
    data.map((intervention,key)=>{   
        interventions[key]={
            id:intervention.id,
            text:intervention.client_name,
            subText:concateIntervenants(intervention),
            time:intervention.date,
            status:getInterventionStatusListObject(intervention),
            info:getFullPeriodText(intervention.period)
        }
    });
    return interventions;
}


