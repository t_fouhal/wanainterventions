import React  from 'react';
import { Link } from 'react-router-dom';




const Pagination=(props)=>{
    let pages=[];
    for (let i=1;i<=props.pagination.last_page;i++){
        pages[i]=i;
    }

    return (
        <ul className="pagination">
            <li className={props.pagination.prev_page_url?"page-item":"page-item disabled"}>
                <Link className="page-link" 
                to={props.pagination.prev_page_url?
                    (props.path+"/"+(props.pagination.current_page-1)):'#'}>
                «
                </Link>
            </li>
            {pages.map((item,key)=>
                                    <li key={key} className={item==props.pagination.current_page?"page-item active":"page-item"}>
                                                         <Link className="page-link"

                                                               to={props.path+"/"+item}>
                                                             {item}
                                                         </Link>
                                    </li>
            )}
            <li className={props.pagination.next_page_url?"page-item":"page-item disabled"}>
                <Link className="page-link" 
                to={props.pagination.next_page_url?
                    (props.path+"/"+(props.pagination.current_page+1)):'#'}>
                »
                </Link>
            </li>
        </ul>
    );
}

export default Pagination;

