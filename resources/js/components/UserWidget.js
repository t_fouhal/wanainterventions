import React ,{Component} from 'react';
import { Link } from 'react-router-dom';



export default class  UserWidget extends Component{

    logout=(e)=>{
        SpinnerStart();
        e.preventDefault();
        axios.post('/logout')
        .then(res=>{
            SpinnerStop();
            window.location.replace(window.globalconf.applicationpath+'/signin')
        })
    }

    render(){
        return (
                <div className="relative">
                    <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false" aria-controls="userSettingsCollapse" className="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
                        <i className="icon icon-cogs" />
                    </a>
                    <div className="user-panel p-3 light mb-2">
                        <div>
                            <div className="float-left image">
                                <img className="user_avatar" src={window.globalconf.applicationpath+this.props.img} alt="User Image" />
                            </div>
                            <div className="float-left info">
                                <h6 className="font-weight-light mt-2 mb-1">{window.globalconf.user.name}</h6>
                            </div>
                        </div>
                        <div className="clearfix" />
                        <div className="collapse multi-collapse" id="userSettingsCollapse">
                            <div className="list-group mt-3 shadow">
                                <Link to={'/users/'+window.globalconf.user.id} className="list-group-item list-group-item-action ">
                                    <i className="mr-2 icon icon-user-circle text-blue" />Profile
                                </Link>
                                <a href='#' onClick={this.logout} className="list-group-item list-group-item-action ">
                                    <i className="mr-2 icon icon-sign-out text-blue" />Déconnexion
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
        );
    }
}


