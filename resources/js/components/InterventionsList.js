import EditableList from './EditableList';


export default class InterventionsList extends EditableList{
    getPaths=()=>{
        return {
           path:this.props.hasOwnProperty('path')?this.props.path:'/interventions-list',
           entityPath:'/interventions?full=true'
           +(this.props.user_id?'&user_id='+this.props.user_id:'')
        }
    }
   
    defineObject=(item)=>{
       return {
           id:item.id,
           text:item.client_name,
           subText:item.hasOwnProperty('intervenants_string')?item.intervenants_string:'',
           info:this.getFullPeriodText(item.period),
           status:this.getStatusObjectsArray(item),
           time:item.date
       }
    }
   
   
    getStatusObjectsArray=(entity)=>{
        switch (entity.status){
            case 0: return {text:'nouvelle',color:'info'};
            case 1: return {text:'completée',color:'success'};
            case 2: return {text:'reportée',color:'danger'}; 
        }
    }
    
    getFullPeriodText=(key)=>{
        switch(key){
            case 'am': return 'demi journée (am)';
            case 'pm': return 'demi journée (pm)';
            case 'j': return 'journée';
        }
    }
}

