import React from 'react';


const DropdownHeader=({data})=>{
    return (
        <div className="dropdown-content-heading">
            {data.hasOwnProperty('info') && data.info && (
                <span className="text-left">{data.info}</span>
            )}
            {data.hasOwnProperty('button') &&
                (<a href={data.button.link}><i className={data.button.icon+" pull-right"}></i></a>)
            }
        </div>
    );
}

export default DropdownHeader;

