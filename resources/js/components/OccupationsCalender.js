import React  from 'react';
import EditableCalender from './EditableCalender';
import Occupation from './Occupation';




export default class OccupationsCalender extends EditableCalender{
   
   
view=(props,click)=>{
    let user=this.props.hasOwnProperty('user')?{user:this.props.user}:{}
    return <Occupation  {...props} {...click}  {...user}/>
}



render(){
    return this.calenderEntity({
        text:'string_status',
        for:this.props.for,
        colorPallet:{
            colorful:'status',
            colors:{
                 0:'#2980b9',
                '-1':'#95a5a6',
                '-2':'#c0392b',
                '-3':'#e67e22',
                '-4':'#9b59b6',
                '-5':'#1abc9c',
        }}
    },this.props.viewParams)
}
   
}