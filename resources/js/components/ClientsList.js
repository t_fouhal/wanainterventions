import EditableList from './EditableList';


export default class ClientsList extends EditableList{

 getPaths=()=>{
     return {
        path:'/clients-list',
        entityPath:'/clients?paginated=true'
     }
 }

 defineObject=(item)=>{
    return {
        id:item.id,
        text:item.name
    }
 }


}

