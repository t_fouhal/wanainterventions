import React ,{Component,Fragment} from 'react';
import 'chart.js';
import getCanvas from './html2canvas';

export default class BarsChart extends Component{
    
    componentDidMount(){
        var labels=[],data=[],datasets=[];
        let groupeditems={}
        for(let item in this.props.data){
            if(groupeditems.hasOwnProperty(this.props.data[item].name)) groupeditems[this.props.data[item].name].push(this.props.data[item])
            else groupeditems[this.props.data[item].name]=[this.props.data[item]]
        }
        let datastatus={nouvelle:[],completee:[],reportee:[]}
        for(let item in groupeditems){          
            labels.push(item)
            let found=[]
            for(let subitem in groupeditems[item]){  
                if(groupeditems[item][subitem].status=='nouvelle') {datastatus.nouvelle.push(groupeditems[item][subitem].count); found.push('nouvelle')}
                if(groupeditems[item][subitem].status=='completée') {datastatus.completee.push(groupeditems[item][subitem].count); found.push('completee')}
                if(groupeditems[item][subitem].status=='reportée') {datastatus.reportee.push(groupeditems[item][subitem].count); found.push('reportee')}
            }
            let stringStatuses=['nouvelle','reportee','completee']
            for(let status in stringStatuses ){
               if(found.indexOf(stringStatuses[status])==-1) datastatus[stringStatuses[status]].push(0)
            }
        }

        var ctx = document.getElementById('graph_bar').getContext('2d');
        var myChart = new Chart(ctx, {
            maintainAspectRatio:false,
           // responsive:true,
            type: 'horizontalBar',
            data: {
                labels: labels,
                datasets: [
                    {data:datastatus.nouvelle,backgroundColor: "#3498db" },
                    {data:datastatus.completee,backgroundColor: "#2ecc71" },
                    {data:datastatus.reportee,backgroundColor: "#e74c3c" }
                ]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                       label: function(tooltipItem) {
                              return tooltipItem.yLabel;
                       }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fixedStepSize: 1,
                      }
                    }]
                },
                events: false,
    tooltips: {
        enabled: false
    },
    hover: {
        animationDuration: 0
    },
    animation: {
        duration: 1,
        onComplete: function () {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];    
                    ctx.fillText(data, bar._model.x+10, bar._model.y);
                });
            });
        }
    }
            },
          
        });
    }
    export=()=>{
        getCanvas(document.getElementById('bars'),this.props.title+' '+this.props.subTitle);
    }

    render(){
        return (
                <Fragment>
                        <div className="card no-r no-b shadow">
                            <div className="card-body p-0">
                            <button onClick={this.export} className="btn btn-primary btn-lg float-right">
                                <i className="icon-save mr-2" />
                                            Exporter
                            </button>
                         </div>
                    </div>
                    <div className="card " id="bars">
                        <div className="card-header white">
                            <strong> {this.props.title} <small>{this.props.subTitle}</small> </strong>
                        </div>
                        <div className="card-body p-0">
                            <canvas id="graph_bar" >    
                            </canvas>
                        </div>
                    </div>
                </Fragment>
        )
    }
}

