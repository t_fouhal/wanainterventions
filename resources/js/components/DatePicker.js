import React,{Component}  from 'react';
import 'jquery-datetimepicker';


export default class DatePicker extends Component{


    componentDidMount(){
        $("#"+this.props.name).datetimepicker({
            timepicker: false,
            
             format: this.props.hasOwnProperty('format')?this.props.format:"Y-m-d",
            onSelect: function(e) {
                $(this).change();
        }
      });
  

    }

    



 render(){   
    return (
        <div className="input-group">
            <input type="text" id={this.props.name} className="date-time-picker form-control"  
             name={this.props.name}
             onChange={this.props.onChange} 
            
             onBlur={this.props.onChange}
             defaultValue={this.props.hasOwnProperty('value') && this.props.value ?this.props.value:''}
             />
            <span className="input-group-append">
            <span className="input-group-text add-on white">
                <i className="icon-calendar" />
            </span>
            </span>
      </div>
    );
}
}

