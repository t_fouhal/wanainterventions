import React from 'react';
import NavbarMenu from './NavbarMenu';
import NotificationsBell from './NotificationsBell';


const Header=(props)=>{
    return (
        <div className="has-sidebar-left">
            <div className="sticky">
                <div className="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                    <div className="relative">
                        <a href="#" data-toggle="offcanvas" className="paper-nav-toggle pp-nav-toggle">
                            <i></i>
                        </a>
                    </div>
                    <NavbarMenu>
                        <NotificationsBell  
                        count={4}
                        items={[
                            {link:"#",text:'same notification',icon:'icon-file'},
                            {link:"#",text:'same notification',icon:'icon-file'},
                            {link:"#",text:'same notification',icon:'icon-file'}
                        ]}
                        />
                    </NavbarMenu>
                </div>
            </div>
        </div>
        );
}

export default Header;

