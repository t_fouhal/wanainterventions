import React ,{Fragment,Component} from 'react';
import List from './List';


export default class EditableList extends Component{

    state={
        collection:[],
        pagination:null,
        forceUpdate:false
    }

    componentDidMount(){
        this.getCollection()
    }
    getCollection=()=>{
        let page=this.props.hasOwnProperty('page') && this.props.page ?
        this.props.page:
        (this.props.match?(this.props.match.params.page?this.props.match.params.page:'1'):'1')
        
        let  addQueryString=this.getPaths().entityPath.indexOf('?')==-1?'?':'&';
        SpinnerStart()
         return axios.get('/api'+this.getPaths().entityPath
         +addQueryString+'page='+page)
         .then(({data})=>{
                let collection=this.getCollectionObjectsArray(data.data.data);
                SpinnerStop()
                this.setState({data:data.data.data,
                    collection,pagination:data.data,currentPage:window.location.pathname})
            })
     
    }


    shouldComponentUpdate(){
        return this.state.currentPage != window.location.pathname || this.state.forceUpdate;
    }
    componentDidUpdate(){
        this.getCollection()
    }
    itemDeleted=()=>{
        this.setState({forceUpdate:true})
        this.getCollection().then(res=>{
            this.setState({forceUpdate:false})
        })

    }
    getCollectionObjectsArray=(data)=>{
        let collection=[];
        data.map((item,key)=>{   
            collection[key]=this.defineObject(item)
        });
        return collection;
    }
    


render(){
    let buttonsListeners=this.props.hasOwnProperty('buttonsListeners')?this.props.buttonsListeners:{}
    if(this.state.collection)  
    return <List
                    items={this.state.collection}
                    pagination={this.state.pagination}
                    {...this.getPaths()}
                    {...buttonsListeners}
                   deleted={this.itemDeleted}
                />
     else return <Fragment></Fragment>
}
}

