import React ,{Component,Fragment} from 'react';
import  '@fullcalendar/core';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';




export default class TimelineCalender extends Component{
    

    componentDidMount(){
        this.createCalender()
    }


    createCalender=()=>{
        $('#calendar').fullCalendar({
            plugins: [ resourceTimelinePlugin ],
            defaultView: 'resourceTimeline',
            resources: [
                { id: 'a', title: 'Auditorium A' }
            ],
            events: [
                { id: '1', resourceId: 'a', start: '2017-08-07T02:00:00', end: '2017-08-07T07:00:00', title: 'event 1' },
            ]
          });

        
    }


    render() {
        return (
            <div className="animatedParent animateOnce">
            <div className="container-fluid p-0" >
       
                <div className="row no-gutters">
                    <div className="col-md-10 offset-md-1">
                        <div className="card no-r no-b shadow">
                            <div className="card-body p-0">
                                <button  className="btn btn-primary btn-lg float-right">
                                <i className="icon-save mr-2" />
                                Exporter
                                </button>
                                <div id="calendar"  />        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}