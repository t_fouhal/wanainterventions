import  html2canvas from 'html2canvas';

let saveCanvas=(url, filename)=> {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        link.href = url;
        link.download = filename;
        //Firefox requires the link to be in the body
        document.body.appendChild(link);
        //simulate click
        link.click();
        //remove the link when done
        document.body.removeChild(link);
    } else {
        window.open(url);
    }
}

let getCanvas=(section,defaultname)=>{
    html2canvas(section).then(canvas => {
        let filename=prompt("nom de fichier:", defaultname);
        if(filename != null || filename.trim()!=''){ 
            saveCanvas(canvas.toDataURL(), filename+'.png');
            toastr.success('le fichier a été sauvgardé avec succés')
        }else toastr.warning(' l\'opération a été anullé')
        
    });
}

export default getCanvas;