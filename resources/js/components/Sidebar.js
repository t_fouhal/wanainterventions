import React ,{Fragment} from 'react';
import SidebarItem from './SidebarItem';
import SidebarItemsGroup from './SidebarItemsGroup';
import UserWidget from './UserWidget';

const Sidebar=(props)=>{

    let restrictedInterventionsItemsGroup=[
        {title:'List des interventions',link:'/interventions-list',icon:'icon icon-list2'},
        {title:'Calendrier',link:'/interventions-calender',icon:'icon icon-calendar'}
    ]

    return (

        <aside className="main-sidebar fixed offcanvas shadow">
            <section className="sidebar">
                <div className="w-80px mt-3 mb-3 ml-3">
                    <img src={window.globalconf.applicationpath+"/assets/img/logo.png"}  />
                </div>
                <UserWidget
                    img={"/assets/img/"+window.globalconf.user.gender+".png"}
                />
                <ul className="sidebar-menu">
                <SidebarItemsGroup
                    icon="icon icon-tasks"
                    title="les interventions"
                    items={window.globalconf.user.is_admin?
                        restrictedInterventionsItemsGroup
                        .concat([{title:'Créer une interventions',link:'/create-intervention',icon:'icon icon-add'}])
                        :restrictedInterventionsItemsGroup
                    }
                    />
                {window.globalconf.user.is_admin ?
                <Fragment>
                    <SidebarItemsGroup
                    icon="icon icon-users"
                    title="Les intervenants"
                    items={[
                        {title:'Créer un intervenant',link:'/create-user' ,icon:'icon icon-add'},
                        {title:'List des intervenants',link:'/users-list',icon:'icon icon-list2'}
                    ]}
                    />
               
                   <SidebarItemsGroup
                    icon="icon icon-schedule"
                    title="Timesheet"
                    items={[
                        {title:'ajouter dans le Timesheet',link:'/create-occupation',icon:'icon icon-add'},
                        {title:"List des Occupations",link:'/occupations-list',icon:'icon icon-list2'},
                        {title:'Calendrier',link:'/timesheet',icon:'icon icon-calendar'}
                    ]}
                    />
                    <SidebarItemsGroup
                    icon="icon icon-building-o"
                    title="Les clients"
                    items={[
                        {title:'Créer un client',link:'/create-client',icon:'icon icon-add'},
                        {title:'List des clients',link:'/clients-list',icon:'icon icon-list2'},
                    ]}
                    />
                    <SidebarItemsGroup
                    icon="icon icon-book"
                    title="Les contrats"
                    items={[
                        {title:'Créer une contrats',link:'/create-contract',icon:'icon icon-add'},
                        {title:'List des contrats',link:'/contracts-list',icon:'icon icon-list2'},
                    ]}
                    />
                    <SidebarItemsGroup

                    icon="icon icon-analytics-1"
                    title="Statistiques"
                    items={[
                        {title:'Statistiques en temps réel',link:'/analytics',icon:'icon icon-av_timer'}
                    ]}
                    />
               </Fragment>
                :null
                }
                </ul>
            </section>
        </aside>
    );
}

export default Sidebar;