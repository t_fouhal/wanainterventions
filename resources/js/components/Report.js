import React ,{Component,Fragment} from 'react';
import DatePicker from './DatePicker';


export default class Report extends Component{

    state={
        data:null,
        downloadAvailable:false,
        action:null
    }
    componentDidMount() {
        this.setState({action:'/api/reports'
                                +(this.props.method=='PUT'?
                                    ('/'+this.props.id):
                                        ('?contract_id='+this.props.contract))})
        
      this.handlePutMethod();
    }
    

    onChange=(e)=>{
        let data={};
        for(let input in this.state.data){
            data[input]=this.state.data[input];
        }
        data[e.target.name]=e.target.value;
        this.setState({data})
    }
    submit=(e)=>{
        if(this.state.data 
            && this.state.data.hasOwnProperty('destination') 
            && this.state.data.destination  
        ){
            if((this.props.method=='POST' && this.state.data.hasOwnProperty('date')  && this.state.data.date)
                || this.props.method=='PUT' )  return;
        }
        e.preventDefault();
        SpinnerStart();
        let method=this.props.method.toLowerCase();
        axios[method](this.state.action,  this.state.data)
        .then((res)=>{
                SpinnerStop()
                toastr.success('le rapport '+(method=='put'?'modifiée':'crée')+' avec succés')
                if(this.props.hasOwnProperty('as_model')) this.props.refreshContract()
        }).catch((err)=>{
            SpinnerStop()
            toastErrors(err)
        })
    }
    handlePutMethod=()=>{
        if(this.props.method=='PUT'){
            SpinnerStart()
            axios.get('/api/reports/'+this.props.id)
                .then(({data})=>{
                 this.setState({downloadAvailable:data.data.destination})
                  document.querySelector("input[name=date]").value=data.data.date
                  document.querySelector("input[name=destination]").value=data.data.destination
                  SpinnerStop()
                }).catch(err=>{
                    SpinnerStop()
                    toastErrors(err)
                })
        }
    }

    render(){
          return    <div className={"col-md-"+(this.props.hasOwnProperty('as_model')?'12':'7')+" offset-md-2"}>
                        <form onSubmit={this.submit}
                            onChange={this.onChange}
                            onSelect={this.onChange}
                            action={window.globalconf.applicationpath+this.state.action}
                            method="POST"
                            autoComplete="off"
                            enctype="multipart/form-data"
                            >
                            {this.props.method=='PUT'&& 
                            <Fragment>
                                <input type="hidden" name="_method" value="PUT" />
                                <input type="hidden" name="_token" value={document.head.querySelector('meta[name="csrf-token"]').content} />
                            </Fragment>
                            }
                            <div className="card no-b  no-r" >
                                <div className="card-body" style={this.props.hasOwnProperty('as_model')?{border:'2px solid #bdc3c7'}:{}}>
                                <div className="form-row">
                                    <div className="col-md-12">

                                    <div className="form-row">
                                    
                                        <div className={"form-group col-7 m-0"}>
                                            <label htmlFor="destination" className="col-form-label s-12"><i className="icon-file mr-2" />Le fichier</label>
                                            <input type='file' className="form-control r-0 light s-12 " name='destination'/>                                            
                                        </div>
                                        <div className="form-group col-5 m-0">
                                            <label htmlFor="date" className="col-form-label s-12"><i className="icon-time mr-2" />La date</label>
                                            <DatePicker name='date' onChange={this.onChange}/>
                                        </div>
                                    </div>
                                    {this.state.downloadAvailable &&
                                        <div className="form-group col-12 m-0">
                                           <a target='_blank' href={window.globalconf.applicationpath+this.state.downloadAvailable}><i className="icon icon-download my-5 mr-2 display-5"  /> {this.state.downloadAvailable} </a>
                                        </div>
                                    }
                                </div>

                                </div>  
                                <hr />
                                <div className="card-body">
                                        <button type="submit" className="btn btn-primary btn-lg"><i className="icon-save mr-2" />Enregistrer</button>
                                </div>  
                                </div>
                               

                            </div>
                        </form>
                    </div>
    }
}

