import React ,{Fragment,Component} from 'react';
import '../libs/jquery.responsivetabs';



export default class Tabs extends Component{
    componentDidMount(){
        $('.responsive-tab').responsiveTabs();
    }
    render() {
        return (
            <Fragment>
                <div className="row">
                    <ul className="nav nav-material nav-material-white responsive-tab" id="v-pills-tab"
                        role="tablist">
                        {this.props.items.map((item, key) =>{
         

                        return     (<li key={key}>
                                        <a className={key == 0 ? "nav-link active" : "nav-link"}
                                        id={'v-pills-'+item.id+"-tab"} data-toggle="pill"
                                        href={"#v-pills-" +item.id} role="tab"
                                        aria-controls={"v-pills-"+item.id}
                                        aria-selected={key==0?"true":"false"}
                                        >
                                            <i className={item.icon}/>
                                        
                                            {item.title}
                                        </a>
                                    </li>)
                        }
                        )
                        }
                    </ul>
                </div>

            </Fragment>
        );
    }
}

