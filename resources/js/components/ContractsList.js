import EditableList from './EditableList';


export default class ContractsList extends EditableList{

 getPaths=()=>{
     return {
        path:this.props.hasOwnProperty('path')?this.props.path:'/contracts-list',
           entityPath:'/contracts?paginated=true'
           +(this.props.client_id?'&client_id='+this.props.client_id:'')
     }
 }

 defineObject=(item)=>{
    return {
        id:item.id,
        text:item.client_name,
        time:item.start+' a '+item.end,
        info:item.days_nbr.replace(/\.?0+$/, '')+' journée'+(item.days_nbr>1?'s':'')+' '+this.getFullFrequencyText(item.frequency),
        status:item.is_active?{text:'active',color:'success'}:{}
    }
 }

 getFullFrequencyText=(key)=>{
    switch(key){
        case 'm': return 'chaque mois';
        case 'w': return 'chaque semaine';
        case 'd': return 'chaque jour';
        case 'y': return 'chaque année';
    }
}


}

