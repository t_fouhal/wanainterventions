import React ,{Component} from 'react';


export default class Login extends Component{


    login=(e)=>{
        SpinnerStart()
        e.preventDefault();
        axios.post('/login',this.state)
        .then(res=>{
            window.location.replace(window.globalconf.applicationpath)
        }).catch((err=>{
            SpinnerStop()
            toastErrors(err)
        }))
        
    }
    onChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }

    render(){    
        return (
                <main>
                <div id="primary" className="p-t-b-100 height-full ">
                <div className="container">
                    <div className="row">
                    <div className="col-lg-4 mx-md-auto">
                        <div className="text-center">
                        <img src={window.globalconf.applicationpath+'/assets/img/logo.png'} />
                        <h3 className="mt-2">Wana Interventions</h3>
                        </div>
                        <form action="dashboard2.html" onSubmit={this.login}  onChange={this.onChange} >
                        <div className="form-group has-icon"><i className="icon-envelope-o" />
                            <input type="text" className="form-control form-control-lg" placeholder="Adresse Email" name="email"/>
                        </div>
                        <div className="form-group has-icon"><i className="icon-user-secret" />
                            <input type="password" className="form-control form-control-lg" placeholder="Mot de passe" name="password"/>
                        </div>
                        <div className="form-group has-icon"> 
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                <div>
                                Se souvenir moi
                                </div>
                                <div className="material-switch">
                                <input id="remember" name="remember" type="checkbox" />
                                <label htmlFor="remember" className="bg-primary"  />
                                </div>
                            </li>
                        </div>
                        <input  type="submit" className="btn btn-success btn-lg btn-block" value="S'identifier" />
                       
                        </form>
                    </div>
                    </div>
                </div>
                </div>
                {/* #primary */}
            </main>
            );
        }
}
