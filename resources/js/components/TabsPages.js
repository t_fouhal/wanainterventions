import React  from 'react';


const TabsPages=(props)=>{
    return (
         <div className="container-fluid animatedParent animateOnce my-3">
            <div className="animated fadeInUpShort go">
                <div className="tab-content" id="v-pills-tabContent">
                    {props.pages.map((page,key)=>
                        <div key={key} className={key==0?"tab-pane fade show active":"tab-pane fade"}
                             id={"v-pills-"+page.id} role="tabpanel"
                             aria-labelledby={"v-pills-"+page.id+"-tab"}>
                            <div className="row">

                              {(page.view)}

                            </div>
                        </div>
                    )}
            </div>
         </div>
        </div>
    );
}

export default TabsPages;



