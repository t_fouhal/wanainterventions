import React,{Component} from 'react'
import { Link } from 'react-router-dom';

export default class NotificationBell extends Component{
    state={
        data:[]
    }

    componentDidMount() {
        axios.get('/api/reports?for_notifications=true')
        .then(({data})=>{
            let notifications=data.data.map((item,key)=>{
                return {
                    link:'/reports/'+item.id,
                    text:item.client_name,
                    info:'rapport a envoyer',
                    icon:'icon-file'
                }
            })
            console.log(notifications)
            this.setState({data:notifications})
        })

    }
    

    render(){
        let count=this.state.data.length?this.state.data.length:''
        return  <li className="dropdown custom-dropdown notifications-menu">
                    <a href="#" className=" nav-link" data-toggle="dropdown" aria-expanded="false">
                        <i className="icon-notifications "></i>
                        <span className="badge badge-danger badge-mini rounded-circle">{count}</span>
                    </a>
                    <ul className="dropdown-menu">
                        <li className="header">{"vous avez "+(count?count:'acune')+" notifications" }</li>
                        {this.state.data.length ?
                        this.state.data.map((item,key)=>{
                            return <li key={key}>
                                        <ul className="menu">
                                            <li>
                                                <Link to={item.link}>
                                                    <i className={"icon "+item.icon+" text-primary"}></i> {item.text}
                                                    <small> {item.info}</small>
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                        })
                        :null
                    }
                        {this.state.data.length>3?
                        <li className="footer p-2 text-center"><a href="#">afficher le tous</a></li>:null
                        }
                    </ul>
                </li>
       
    }
}
