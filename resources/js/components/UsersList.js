import EditableList from './EditableList';


export default class UsersList extends EditableList{

 getPaths=()=>{
     return {
        path:'/users-list',
        entityPath:'/users?paginated=true'
     }
 }

 defineObject=(item)=>{
    return {
        id:item.id,
        text:item.name,
        subText:'',
        status:this.getStatusObjectsArray(item)
    }
 }


 getStatusObjectsArray=(entity)=>{
     return entity.is_admin?{text:'Administrateur',color:'success'}:{text:'Utilisateur',color:'info'}
 }

}

