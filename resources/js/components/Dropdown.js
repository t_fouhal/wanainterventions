import React , {Fragment} from 'react';
import DropdownHeader from './DropdownHeader';


const Dropdown=(props)=>{
    return (
        <Fragment>
            {props.hasOwnProperty('header') && (
                <DropdownHeader data={props.header}  />
            )}
            <div className="dropdown-content-body">
                <ul>
                {props.items.map((item)=>{
                    return (
                        <li><a href={item.hasOwnProperty('link')?item.link:"#"}>
                            {item.hasOwnProperty('icon') && (<i className={item.icon}></i>)}
                            {item.hasOwnProperty('img')  && (<img className="pull-left m-r-10 avatar-img" src={item.img} />)}
                            {item.hasOwnProperty('text') && (<span>{item.text}</span>)}
                            {item.hasOwnProperty('block') ? item.block : null}
                        </a></li>
                    )
                })}
                </ul>
            </div>
        </Fragment>
    );
}

export default Dropdown;

