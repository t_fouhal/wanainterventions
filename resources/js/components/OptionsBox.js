import React ,{Fragment} from 'react'

const OptionsBox=(props)=>{
    return  props.options.map((item,key)=>{
                    return  <div className="card" key={key} style={{cursor:'pointer'}}>
                                <div className="card-body p-0">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item"  onClick={item.onClick}>
                                            <i className={"icon "+item.icon+" text-primary"}></i> {item.text}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                })
}

export default OptionsBox;