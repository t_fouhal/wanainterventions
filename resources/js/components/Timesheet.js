import React ,{Fragment,Component} from 'react';
import OccupationsCalender from './OccupationsCalender';
import CalenderExporter from './CalenderExporter';
import OccupationsList from './OccupationsList';
import Select2 from './Select2'
import Model from './Model'


export default class Timesheet extends Component{
    state={
        users:[],
        selectedUser:null,
        showModel:false,
        mode:'calender'
    }
    componentDidMount(){
        axios.get('/api/users').then(({data})=>{
            let users=[];
            for(let user in data.data){
                users.push({
                    text:data.data[user].name,
                    id:data.data[user].id
                })
            }
            this.setState({users,selectedUser:users[0]});
        })
    }
    switchUser=(e)=>{
        let userObject={}
        rhistory.push('/timesheet')
        for(let user in this.state.users){
            if(this.state.users[user].id==e.currentTarget.id) userObject=this.state.users[user]
        }
        this.setState({selectedUser:userObject})
  
    }
    
    changeMode=(e)=>{
        this.setState({mode:e.target.value})
    }

    render(){
        
        return  <div className='row'>
                {this.state.showModel &&
                <Model onClick={()=>this.setState({showModel:false})}>
                    <CalenderExporter />
                </Model>
                }
                {this.state.users.length &&
                <Fragment>
                    <div className="col-2">
                    <Select2 
                    id='mode'
                    name='mode'
                    items={[
                        {key:'Calendrier',value:'calender'},
                        {key:'Liste',value:'list'},
                    ]}
                    selected={[this.state.mode]}
                    onChange={this.changeMode}
                    />
                    <button onClick={()=>this.setState({showModel:true})} className="btn btn-primary btn-lg mt-2">
                            <i className="icon-save mr-2" />
                            Exporter tout
                    </button>
                    
                        <div className="card no-b shadow mt-2" >
                        <div className="card-header  light">
                                <h6>Les Intervenants</h6>
                            </div>
                            <div className="card-body slimScroll p-0" data-height="10">
                                <div className="table-responsive"  >
                                    <table className="table table-hover " style={{maxHeight:'30px',overflowY:'scroll'}}>
                                        <tbody>
                                        {this.state.users.map((item,key)=>
                                            <tr onClick={this.switchUser}
                                                 style={{cursor:'pointer'}}
                                                className={this.state.selectedUser.id==item.id?' bg-primary text-white':''}
                                                key={key} id={item.id} >
                                                <td>
                                                    <h6>{item.text}</h6>
                                                </td>
                                            </tr>
                                        )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-10">
                    {this.state.selectedUser && this.state.mode=='calender' &&
                    <OccupationsCalender key={this.state.selectedUser.id} 
                    for={"occupations?user="+this.state.selectedUser.id}  
                    user={this.state.selectedUser.id} 
                    viewParams={{fullWidth:true}}
                        fileAppendName={this.state.selectedUser.text}/>
                    }
                    {this.state.selectedUser && this.state.mode=='list' &&
                    <OccupationsList
                        key={this.state.selectedUser.id}
                        page={this.props.match.params.page}
                        path='/timesheet'
                        user_id={this.state.selectedUser.id} 
                    />
                    }
                    </div>
                    
                </Fragment>  

                }
                </div>
        
    }
    
}

