import React ,{Fragment , Component} from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import ReactDOM from "react-dom";
import './common';
import Layout from "./Layout";
import Intervention from "./Intervention";
import Client from "./Client";
import Contract from "./Contract";
import User from './User';
import Login from "./Login";
import Register from "./Register";

import UserCreation from './UserCreation';
import InterventionCreation from './InterventionCreation';
import Occupation from './Occupation';
import InterventionsList from './InterventionsList';
import ClientsList from './ClientsList';
import ContractsList from './ContractsList';
import InterventionsCalender from './InterventionsCalender';
import ClientProfile from './ClientProfile';
import UsersList from './UsersList';
import Analytics from './Analytics';
import Timesheet from './Timesheet';
import Report from './Report';
import OccupationsList from './OccupationsList';
//import TimelineCalender from './TimeLineCalender';



class App extends Component{

    componentWillMount(){
        if(!document.head.querySelector('meta[name="author"]') || document.head.querySelector('meta[name="author"]').content!="Fouhal Mohammed Taqi eddine, takifouhal@gmail.com"){
            throw new Error("Application copyright not found! , application is lisenced to Fouhal Mouhammed Taqi eddine takifouhal@gmail.com")
        }
    }

    render(){
        return (
      <Router basename={window.globalconf.applicationpath}>
        <Fragment>
                <Layout>
                    <Route exact path='/' render={()=><InterventionsCalender for="interventions"/>}/>
                    <Route exact path='/interventions-calender' render={()=><InterventionsCalender for="interventions"/>}/>

        {/* <Route exact path='/timeline' component={TimelineCalender}/>*/}

            {window.globalconf.user.is_admin ?
                <Fragment>
                    <Route path='/analytics' component={Analytics}/>
                    <Route path='/timesheet/:page?' component={Timesheet}/>
                    <Route   path='/interventions-list/:page?' component={InterventionsList}/>
                    <Route   path='/occupations-list/:page?' component={OccupationsList}/>
                    <Route   path='/users-list/:page?' component={UsersList}/>
                    <Route   path='/clients-list/:page?' component={ClientsList}/>
                    <Route   path='/contracts-list/:page?' component={ContractsList}/>
                    <Route exact path='/create-occupation' render={()=> <Occupation  method="POST" />}/>
                    <Route exact path='/create-intervention' render={()=><InterventionCreation  method="POST" />}/>
                    <Route exact path='/create-user' render={()=><UserCreation method='POST'/>}/>
                    <Route  path='/create-client' render={()=><Client  method='POST'/>}/>
                    <Route  path='/create-contract' render={()=><Contract  method='POST'/>}/>

                    <Route  path='/interventions/:id' render={(props)=><InterventionCreation id={props.match.params.id} method='PUT'/>}/>
                    <Route  path='/contracts/:id' render={(props)=><Contract id={props.match.params.id} method='PUT'/>}/>
                    <Route  path='/clients/:id/:page?' component={ClientProfile}/>
                    <Route  path='/reports/:id' render={(props)=><Report method='PUT' id={props.match.params.id} />}/>
                    <Route  path='/occupations/:id' render={(props)=><Occupation method='PUT' id={props.match.params.id} />}/>
                </Fragment> :null 
            }
                    <Route  path="/users/:id/:page?"  component={User} />

                </Layout>
        </Fragment>
    </Router>
        );
    }
}

if (window.location.pathname==window.globalconf.applicationpath+"/signin") {
    ReactDOM.render(<Login />, document.getElementById('app'));
}else ReactDOM.render(<App />, document.getElementById('app'));



