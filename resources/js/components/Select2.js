import React ,{Component} from 'react';
import 'select2';


export default class Select2 extends Component{


    componentDidMount(){
        $('#'+this.props.id).select2();
        $('#'+this.props.id).on('change', (e)=>{
           if(this.props.hasOwnProperty('multiple')){
             let value=[];
            for(let option in e.currentTarget.selectedOptions){
                if(e.currentTarget.selectedOptions[option].value){
                    value.push(e.currentTarget.selectedOptions[option].value)
                }
            }
            this.props.onChange({
                target:{
                    value,name:e.target.name
                }   
            });
        }else  this.props.onChange(e);
          });
    }
    componentDidUpdate(){
        $('#'+this.props.id).select2();


    }
    in_array=(needle,array)=>{
        for(let item in array){
            if(array[item]==needle) return true;
        }
        return false;
    }

  

    render(){   

        let opts={}
        if(this.props.hasOwnProperty('multiple')){
            opts={multiple:'multiple'}
        }

        

        return (
            <select 
                name={this.props.hasOwnProperty('multiple')?this.props.name+'[]':this.props.name} 
                className="select2"  id={this.props.id} {...opts}
             >
             {!this.props.hasOwnProperty('multiple') && <option value=''>selectionnez ...</option>}
                {this.props.items.map((item,key)=>{
                   // let selected=this.props.hasOwnProperty('selected') && $.inArray(item.value,this.props.selected)?{selected:'selected'}:{}
                    return (<option key={key}
                                value={item.value}  
                                selected={this.props.hasOwnProperty('selected') && 
                                this.in_array(item.value,this.props.selected)}
                                >{item.key}</option>)
                }
                )
                }
            </select>
        );
    }    
}



