const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
let webpack = require('webpack');

/*let JavaScriptObfuscator = require('webpack-obfuscator');

mix.webpackConfig({
    plugins: [
        new JavaScriptObfuscator ({
            stringArrayEncoding: false,
            identifierNamesGenerator: 'hexadecimal',
            rotateStringArray: true,
            renameGlobals: true,
            target: 'browser'
        })
    ],
});*/

mix.react('resources/js/app.js', 'public/js');