<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
    return view('main');
});*/


//TODO: soft deletes




Route::get('/test',function(){

   dd(date("d/m", strtotime("2019-04-03")));//
//............
});

\Illuminate\Support\Facades\Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/signin',function(){
    if(!Auth::user()){
        return view('main')
        ->withGlobalconf(json_encode(["applicationpath"=>env('APPLICATION_PATH')]));;
    }else return redirect('/');
    
});


Route::get('/{any_other_page}',function(){
    $gc=null;
    if(Auth::user()){
        $gc=Auth::user();
        return view('main')
        ->withGlobalconf(json_encode(["user"=>$gc,
                                       "applicationpath"=>env('APPLICATION_PATH')
                                ]));
    }else return redirect('/signin');
  
})->where('any_other_page','.*');
