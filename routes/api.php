<?php

use Illuminate\Http\Request;
use App\Http\Middleware\IsAdmin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/clients','ClientController');
Route::resource('/reports','ReportController');

Route::resource('/documents','DocumentController');
Route::resource('/contracts','ContratController');
Route::get('/interventions/count','InterventionController@indexCount');
Route::get('/journees/count','InterventionController@daysCount');

Route::resource('/interventions','InterventionController');

Route::resource('/occupations','TimesheetController');
Route::post('/occupations/export','TimesheetController@export');


//Route::get('/interventions')
//Route::resource('/timesheets','OccupationController');
Route::resource('/users','IntervenantController');
Route::get('/users/occupations/{id}','IntervenantController@occupations');



Route::resource('/intervenants','IntervenantController');
Route::get('/intervenants/occupations/{id}','IntervenantController@occupations');

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/






